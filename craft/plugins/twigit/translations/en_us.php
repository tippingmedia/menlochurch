<?php

return array(
    'Posted' => 'Posted',
    '{pre}.now' => 'just now',
    '{pre}.past.day' => '{pre} {days} day ago',
    '{pre}.past.days' => '{pre} {days} days ago',
    '{pre}.past.minute' => '{pre} {m} minute ago',
    '{pre}.past.minutes' => '{pre} {m} minutes ago',
    '{pre}.past.hour' => '{pre} {h} hour ago',
    '{pre}.past.hours' => '{pre} {h} hours ago',
    /* future */
    '{pre}.future.day' => '{pre} {days} day',
    '{pre}.future.days' => '{pre} {days} days',
    'now' => 'just now',
    '{pre}.future.minute' => '{pre} {m} minute',
    '{pre}.future.minutes' => '{pre} {m} minutes',
    '{pre}.future.hour' => '{pre} {h} hour',
    '{pre}.future.hours' => '{pre} {h} hours',
);