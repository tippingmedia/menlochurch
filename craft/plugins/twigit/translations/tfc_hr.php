<?php

return array(
    'Posted' => 'Posted',
    'Posted.now' => 'just now',
    'Posted.past.day' => '{pre} {days} day ago',
    'Posted.past.days' => '{pre} {days} days ago',
    'Posted.past.minute' => '{pre} {m} minute ago',
    'Posted.past.minutes' => '{pre} {m} minutes ago',
    'Posted.past.hour' => '{pre} {h} hour ago',
    'Posted.past.hours' => '{pre} {h} hours ago',
    /* future */
    'In.future.day' => '{pre} {days} day',
    'In.future.days' => '{pre} {days} days',
    'In.future.minute' => '{pre} {m} minute',
    'In.future.minutes' => '{pre} {m} minutes',
    'In.future.hour' => '{pre} {h} hour',
    'In.future.hours' => '{pre} {h} hours',
);