<?php
namespace Craft;
    use Twig_Extension;
    use Twig_Filter_Method;

class TwigitTwigExtension extends \Twig_Extension
{
    protected $env;
    private $format = "M d, Y";
    
    public function getName()
    {
        return 'Twigit Twig Extensions';
    }
    
    public function getFilters()
    {
        return array(
            'relativeDate'  => new Twig_Filter_Method($this, 'relativedate'),
            'parseTwitter'  => new Twig_Filter_Method($this, 'parsetwit'),
            'timeLeft'      => new Twig_Filter_Method($this, 'timeleft'),
            'getSrc'        => new Twig_Filter_Method($this, 'getSrc'),
            'jsonToArray'   => new Twig_Filter_Method($this, 'jsontoarray'),
            'arrayToJSON'   => new Twig_Filter_Method($this, 'arraytojson'),
            'initials'      => new Twig_Filter_Method($this, 'initials'),
            'strToDate'     => new Twig_Filter_Method($this, 'strtodate'),
            'campusSlug'    => new Twig_Filter_Method($this, 'cslug'),
            'array_unique' => new Twig_Filter_Method($this, 'arrayunique')
        );
    }
    
    
    public function getFunctions()
    {
        return array(
            'relativeDate'  => new \Twig_Function_Method($this, 'relativedate'),
            'parseTwitter'  => new \Twig_Function_Method($this, 'parsetwit'),
            'timeLeft'      => new \Twig_Function_Method($this, 'timeleft'),
            'getSrc'        => new \Twig_Function_Method($this, 'getsrc'),
            'jsonToArray'   => new \Twig_Function_Method($this, 'jsontoarray'),
            'arrayToJSON'   => new \Twig_Function_Method($this, 'arraytojson'),
            'initials'      => new \Twig_Function_Method($this, 'initials'),
            'strToDate'     => new \Twig_Function_Method($this, 'strtodate')
        );
    }
    
    public function initRuntime(\Twig_Environment $env)
    {
        $this->env = $env;
    }
    
    public function relativedate($date)
    {
        $now = time();
        $date = date(strtotime($date));
        $diff = $now - $date;

        if ($diff < 60){
            return sprintf($diff > 1 ? '%s seconds ago' : 'a second ago', $diff);
        }

        $diff = floor($diff/60);

        if ($diff < 60){
            return sprintf($diff > 1 ? '%s minutes ago' : 'one minute ago', $diff);
        }

        $diff = floor($diff/60);

        if ($diff < 24){
            return sprintf($diff > 1 ? '%s hours ago' : 'an hour ago', $diff);
        }

        $diff = floor($diff/24);

        if ($diff < 7){
            return sprintf($diff > 1 ? '%s days ago' : 'yesterday', $diff);
        }

        if ($diff < 30)
        {
            $diff = floor($diff / 7);

            return sprintf($diff > 1 ? '%s weeks ago' : 'one week ago', $diff);
        }

        $diff = floor($diff/30);

        if ($diff < 12){
            return sprintf($diff > 1 ? '%s months ago' : 'last month', $diff);
        }

        $diff = date('Y', $now) - date('Y', $date);

        return sprintf($diff > 1 ? '%s years ago' : 'last year', $diff);
    }

    function parsetwit($str)
    {
        $patterns = array();
        $replace = array();
        //parse URL
        preg_match_all("/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.]+/",$str,$urls);
        foreach($urls[0] as $url)
        {
        $patterns[] = $url;
        $replace[] = '<a href="'.$url.'" >'.$url.'</a>';
        }
        //parse hashtag
        preg_match_all("/[#]+([a-zA-Z0-9_]+)/",$str,$hashtags);
        foreach($hashtags[1] as $hashtag)
        {
        $patterns[] = '#'.$hashtag;
        $replace[] = '<a href="http://search.twitter.com/search?q='.$hashtag.'" >#'.$hashtag.'</a>';
        }
        //parse mention
        preg_match_all("/[@]+([a-zA-Z0-9_]+)/",$str,$usernames);
        foreach($usernames[1] as $username)
        {
        $patterns[] = '@'.$username;
        $replace[] = '<a href="http://twitter.com/'.$username.'" >@'.$username.'</a>';
        }
        //replace now
        $str = str_replace($patterns,$replace,$str);
        //
        return $str;
    }

    function timeleft($date){

        $now = new \DateTime("now", new \DateTimeZone(craft()->getTimeZone()));
        $future_date = new \DateTime($date, new \DateTimeZone(craft()->getTimeZone()));

        $interval = $now->diff($future_date);

        if ($interval->m < 1 && $interval->d < 1 && $interval->h && $interval->i < 1) {
            return "";
        } elseif ($interval->m < 1 && $interval->d < 1 && $interval->h < 1) {
            return $interval->format("%i min");
        } elseif ($interval->m < 1 && $interval->d < 1) {
            return $interval->format("%h hrs %i min");
        } elseif ($interval->m < 1) {
            return $interval->format("%d days %h hrs %i min");
        } else {
            return $interval->format("%m mo %d days %h hrs %i min");
        }

    }

    function getsrc($iframe_code){
        $doc = new \DOMDocument();
        $doc->loadHTML($iframe_code);
        $src = $doc->getElementsByTagName('iframe')->item(0)->getAttribute('src');
        return $src;
    }



    function jsontoarray($str){
        $json = json_encode($str);
        return json_decode($json);
    }


    function arraytojson($arry){
        $json = json_encode($arry);
        return $json;
    }

    /*
      Get the first letter of each word and capitalize
      @return string
     */
    function initials($str)
    {
        $expr = '/(?<=\s|^)[a-z]/i';
        preg_match_all($expr, $str, $matches);
        $result = implode('', $matches[0]);
        $result = strtoupper($result);
        return $result;
    }

    function strtodate($str, $format)
    {
        return DateTime::createFromFormat($format, $str);
    }

    function cslug($str)
    {
        return trim(str_replace("houstons-first-","",$str));
    }

    function arrayunique($arry)
    {   
        if (is_array($arry)) {
            return array_unique($arry);
        }
        else
        {
            return [];
        }
    }

    public function timeAgoInWords(\DateTime $date, $format = null) {
        // $timeAgoInWords = DateTimeHelper::timeAgoInWords($datetime);
        // return $timeAgoInWords;
        // https://github.com/eschmar/time-ago-bundle
    
        $diff = time() - $date->format('U');
        $format = $format ? $format : $this->format;
        $pre = $diff > -5 ? Craft::t("Posted") : Craft::t("In");
        $prefix = $pre;
        $prefix .= $diff > -5 ? '.past' : '.future';
        $diff = abs($diff);
        $days = floor($diff / 86400);
        if ($days >= 7) return $date->format($format);
        $dayar = array("pre"=>$pre,"days"=>$days);
        if ($days >= 1) return $days == 1 ? Craft::t($prefix . '.day',$dayar) : Craft::t($prefix . '.days',$dayar);
        if ($diff < 60) return Craft::t($prefix . '.now');
        
        $m = floor($diff / 60);
        $minar = array("pre"=>$pre,"m"=>$m);
        if ($diff < 120) return Craft::t($prefix . '.minute', $minar);
        if ($diff < 3600) return Craft::t($prefix . '.minutes', $minar);

        $h = floor($diff / 3600);
        $hourar = array("pre"=>$pre,"h"=>$h);
        if ($diff < 7200) return Craft::t($prefix . '.hour', $hourar);
        if ($diff < 86400) return Craft::t($prefix . '.hours', $hourar);

        return $date->format($format);
    }

}