<?php
namespace Craft;
 
class TwigitPlugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('Twigit Twig Extension For Various Needs');
    }
    
    public function getVersion()
    {
        return '1.0';
    }
    
    public function getDeveloper()
    {
        return 'Tipping Media';
    }
    
    public function getDeveloperUrl()
    {
        return 'http://www.tippingmedia.com';
    }
    
    public function addTwigExtension()
    {
        Craft::import('plugins.twigit.twigextensions.TwigitTwigExtension');
        
        return new TwigitTwigExtension();
    }
}