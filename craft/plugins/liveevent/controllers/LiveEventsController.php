<?php
namespace Craft;

class LiveEventsController extends BaseController
{
    protected $allowAnonymous = array('getEventsJson');

    public function getEventsJson($source) {
        return craft()->liveEvent_event->liveDateTimesJson($source);
    }
}