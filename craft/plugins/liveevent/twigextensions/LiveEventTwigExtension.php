<?php
namespace Craft;

use \Twig_Extension;
use \Twig_Filter_Method;

class LiveEventTwigExtension extends \Twig_Extension
{
    protected $env;

    public function getName()
    {
        return 'Live Twig Extension';
    }

    public function getFilters()
    {
        return array(
            'livedatetimes' => new \Twig_Filter_Method($this, 'livedatetimes'),
            'livedatetimesjson' => new \Twig_Filter_Method($this, 'livedatetimesjson'),
            'sortDates'         => new \Twig_Filter_method($this, 'sortdates')
        );
    }

    public function getFunctions()
    {
        return array(
            'livedatetimes' => new \Twig_Function_Method($this, 'livedatetimes'),
            'livedatetimesjson' => new \Twig_Function_Method($this, 'livedatetimesjson'),
        );
    }

    public function initRuntime(\Twig_Environment $env)
    {
        $this->env = $env;
    }


    public function livedatetimes($source=array()){
      return craft()->liveEvent_event->liveDateTimes($source);
    }

    public function livedatetimesjson($source=array()){
      return craft()->liveEvent_event->liveDateTimesJson($source);
    }

    public function sortdates($source=array()) {
        usort($source, function($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        return $source;
    }


}
