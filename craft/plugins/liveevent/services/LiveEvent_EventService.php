<?php

/**
 * Craft Live Event by TippingMedia
 *
 * @package   Craft Live Event
 * @author    Adam Randlett
 * @copyright Copyright (c) 2014, TippingMedia
 */


namespace Craft;


class LiveEvent_EventService extends BaseApplicationComponent
{

    protected $todayTZ;
    /**
     * EventsTwigExtension method for grouping by date
     *
     * @return  array
     */
    private function organizeTimes($arr)
    {
      $ul = array();
      $timezone = new \DateTimeZone(craft()->getTimeZone()); //'America/Chicago'
      //Need target timezone below to compare current time to saved time.
      $this->todayTZ = new \DateTime('now', new \DateTimeZone(craft()->getTimeZone()));
      $todaysDateTime = new \DateTime('now', new \DateTimeZone(craft()->getTimeZone()));

      $todaysDayName = strtolower($todaysDateTime->format('l'));

      foreach($arr as $matix => $block)
      {
        
        if($block->type == "liveRepeatEvent") // if repeat event
        {
          $event = array();
          $event['title'] = "";//$block->liveEventTitle;
          //$event['bar'] = $block->toggleLiveBar == 1 ? "1" : "0";
          //If the day of the week is the same as today and current time is less than next events time return today + time
          //else return next day of the week + time
 
          $endDateTime = strtotime($this->todayTZ->format('M d Y') ." ". $block->endTime->format('G:i'));

          $start = "";
          $end = "";
          // $new = array('today'=>$todaysDateTime->format('m/d/y G:i'), 'dayname'=>$endDateTime->format('m/d/y G:i'));
          // \CVarDumper::dump($new, 4, true);
          // exit;


          if($todaysDayName == strtolower($block->dayOfTheWeek)){
             // \CVarDumper::dump($this->todayTZ->format('M d Y G:i'), 4, true);
             // exit;
              if( strtotime($this->todayTZ->format('m/d/Y G:i')) < $endDateTime )
              {

                $start = strtotime($this->todayTZ->format('m/d/Y') ." ". $block->startTime->format('G:i'));
                //$event['end'] = new \DateTime('now', $timezone);
                //$event['end']->setTime(,$block->endTime->format('i'));
                $end = strtotime($this->todayTZ->format('m/d/Y') ." ". $block->endTime->format('G:i') );
                //\CVarDumper::dump($start, 4, true);
                // exit;
              } 
              else 
              {
                
                $start = strtotime($this->todayTZ->format('m/d/Y G:i') . " +1 Week");
                $end = strtotime($this->todayTZ->format('m/d/Y') ." ". $block->endTime->format('G:i') . " +1 Week");

              }

          } 
          else 
          {

            $start = strtotime($block->dayOfTheWeek . " " . $block->startTime->format('G:i'));
            $end = strtotime($block->dayOfTheWeek . " " . $block->endTime->format('G:i'));

          }

          // Current days event
          $sd = new \DateTime(date("m/d/Y G:i",$start), $timezone);
          $sd->setTimezone(new \DateTimeZone('UTC'));
          $ed = new \DateTime(date("m/d/Y G:i",$end), $timezone);
          $ed->setTimezone(new \DateTimeZone('UTC'));
          $event['startDate'] = $sd;
          $event['endDate'] = $ed;
          //$event['start'] = $this->parsedDate($sd);
          //$event['end'] = $this->parsedDate($sd);

          // Current days event
          array_push($ul,$event);


          // Current +1 Week event
          $start = strtotime($event['startDate']->format('c') . " +1 Week");
          $end = strtotime($event['endDate']->format('c') . " +1 Week");

          $event['startDate'] = new \DateTime(date("m/d/Y G:i",$start), new \DateTimeZone('UTC'));
          $event['endDate'] = new \DateTime(date("m/d/Y G:i",$end), new \DateTimeZone('UTC'));
          //$event['start'] = $this->parsedDate(new \DateTime(date("m/d/Y G:i",$start), $timezone));
          //$event['end'] = $this->parsedDate(new \DateTime(date('m/d/Y G:i', $end), $timezone));
          // $event['readable'] = array(
          //     'start' => $event['startDate']->format('M d Y g:i A'),
          //     'end' => $event['endDate']->format('M d Y g:i A'),
          // );

          
          array_push($ul,$event);

        }
        else  // else it is a Single Event
        {

          $event = array();
          $event['title'] = "";//$block->liveEventTitle;

          $start = strtotime($block->startDateTime->format('m/d/Y G:i'));
          $end = strtotime($block->startDateTime->format('m/d/Y') . " " . $block->endTime->format('G:i'));

          if( strtotime($this->todayTZ->format('m/d/Y G:i')) < $end )
          {


            $sd = new \DateTime(date("m/d/Y G:i",$start), $timezone);
            $sd->setTimezone(new \DateTimeZone('UTC'));
            $ed = new \DateTime(date("m/d/Y G:i",$end), $timezone);
            $ed->setTimezone(new \DateTimeZone('UTC'));
            $event['startDate'] = $sd;
            $event['endDate'] = $ed;
            //$event['start'] = $this->parsedDate($sd);
            //$event['end'] = $this->parsedDate($ed);
            // $event['readable'] = array(
            //   'start' => $event['startDate']->format('M d Y g:i A'),
            //   'end' => $event['endDate']->format('M d Y g:i A'),
            // );
            array_push($ul,$event);
          }else{
            continue;
          }

        }
        
      }

      usort($ul, function($a, $b){
        return $a['startDate']->format('U') - $b['startDate']->format('U');
      });

    //  \CVarDumper::dump($ul, 4, true);
    //   exit;

      return $ul;
      
    }

    public function parsedDate($date){
      $month = $date->format('n');
      $day = $date->format('j');
      $year = $date->format('Y');
      $hours = $date->format('G');
      $min = $date->format('i');
      $sec = "0";

      return $year .",". $month .",". $day .",". $hours .",". $min .",". $sec;
    }

    public function liveDateTimes($arr){
      $arry = $this->organizeTimes($arr);
      return $arry;
    }

    public function liveDateTimesJson($arr){
      $arry = $this->organizeTimes($arr);
      $items = array_map(array($this,'flatDate'),$arry);
      return json_encode($items);
    }

    public function flatDate($date) {
       $item = [
         'start' => $date['startDate']->format('c'),
         'end' =>  $date['endDate']->format('c')
       ];
       return $item;
    }



    // private function newDatePlusTime($date1,$date2)
    // {
    //   #
    //   # Extract date parts from $date1 and $date2
    //   $etar = date_parse($date1->format("G:i"));
    //   $star = date_parse($date2->format("Y-m-d"));

    //   #
    //   # Merge time from $date1 to date of $date2
    //   $dateTimeString = $star['year'] . '-' . $star['month'] . '-' . $star['day'] . ' ' . $etar['hour'] . ':' . $etar['minute'];

    //   #
    //   # Generate new Craft DatTime object
    //   $newCraftDateTime = new DateTime($dateTimeString, $timezone);

    //   return $newCraftDateTime;
    // }

}
?>
