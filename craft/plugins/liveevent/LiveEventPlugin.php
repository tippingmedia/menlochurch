<?php
namespace Craft;

class LiveEventPlugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('Live Event organizes array of dates returns next event or event happening now');
    }

    public function getVersion()
    {
        return '1.0';
    }

    public function getDeveloper()
    {
        return 'Tipping Media';
    }

    public function getDeveloperUrl()
    {
        return 'http://www.tippingmedia.com';
    }

    public function addTwigExtension()
    {
        Craft::import('plugins.liveevent.twigextensions.LiveEventTwigExtension');

        return new LiveEventTwigExtension();
    }
}
