<?php
namespace Craft;
/**
 * Layouts helper class.
 *
 * @author    Tipping Media LLC. <support@tippingmedia.com>
 * @copyright Copyright (c) 2016, Tipping Media LLC.
 * @see       http://tippingmedia.com
 * @package   layouts.helpers
 * @since     2.0
 */

class LayoutsHelper
{
    public static function defaultLayouts()
    {
        $string = file_get_contents(CRAFT_PLUGINS_PATH. "layouts/helpers/layouts.json");

        $json_a = json_decode($string, true);
        return $json_a;
    }
}
