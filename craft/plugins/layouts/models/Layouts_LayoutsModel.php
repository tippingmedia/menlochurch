<?php
/**
 * Layouts plugin for Craft CMS
 *
 * Layouts_Layouts Model
 *
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

namespace Craft;

class Layouts_LayoutsModel extends BaseModel
{

    public function __toString()
     {
         return $this->name;
     }
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array(
            'id'            => array(AttributeType::Number, 'default' => null),
            'handle'        => array(AttributeType::String, 'default' => null),
            'description'   => array(AttributeType::String, 'default' => null),
            'icon'          => array(AttributeType::String, 'default' => null),
            'entries'       => array(AttributeType::Mixed, 'default' => null),
        );
    }


    public function getSources()
    {
        if (is_array($this->entries))
        {
            if (array_key_exists('sources', $this->entries))
            {
                if (is_array($this->entries['sources']))
                {
                    return $this->entries['sources'];
                }
            }
        }

        return array();
    }

    public function getEntryTypes()
    {
        if (is_array($this->entries))
        {
            if (array_key_exists('entrytypes', $this->entries))
            {
                if (is_array($this->entries['entrytypes']))
                {
                    $entrytypeIds = array();
                    foreach ($this->entries['entrytypes'] as $value)
                    {
                        $type = explode(':', $value);
                        $entrytypeIds[] = $type[1];
                    }
                    return $entrytypeIds;
                }
            }
        }

        return array();
    }

    public function getLimit()
    {
        if (is_array($this->entries))
        {
            if (array_key_exists('limit', $this->entries))
            {
                if (isset($this->entries['limit']))
                {
                    return $this->entries['limit'];
                }
            }
        }
        return "";
    }

}
