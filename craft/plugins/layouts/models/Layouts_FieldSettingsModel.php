<?php
/**
 * Layouts plugin for Craft CMS
 *
 * Layouts_Layouts Model
 *
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

namespace Craft;

class Layouts_FieldSettingsModel extends BaseModel
{

    /**
	 * @var
	 */
	private $_layouts;

    public function __toString()
     {
         return $this->name;
     }


    /**
	 * Sets the layouts.
	 *
	 * @param array $layouts
	 *
	 * @return null
	 */
	public function setLayouts($layouts)
	{
		$this->_layouts = $layouts;
	}

    /**
	 * Returns the layouts.
	 *
	 * @return array
	 */
	public function getLayouts()
	{
		if (!isset($this->_layouts))
		{
			$this->_layouts = array();
		}

		return $this->_layouts;
	}


    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array(
            'layouts'       => array(AttributeType::Mixed, 'default' => null)
        );
    }

}
