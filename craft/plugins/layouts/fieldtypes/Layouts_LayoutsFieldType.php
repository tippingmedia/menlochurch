<?php
/**
 * Layouts plugin for Craft CMS
 *
 * Layouts_Layouts FieldType
 *
 * --snip--
 * Whenever someone creates a new field in Craft, they must specify what type of field it is. The system comes with
 * a handful of field types baked in, and we’ve made it extremely easy for plugins to add new ones.
 *
 * https://craftcms.com/docs/plugins/field-types
 * --snip--
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

namespace Craft;

class Layouts_LayoutsFieldType extends BaseFieldType
{
    /**
     * Returns the name of the fieldtype.
     *
     * @return mixed
     */
    public function getName()
    {
        return Craft::t('Layouts');
    }

    /**
     * Returns the content attribute config.
     *
     * @return mixed
     */
    public function defineContentAttribute()
    {
        return array(AttributeType::Mixed);
    }


    /**
     * Defines the settings.
     *
     * @access protected
     * @return array
     */
    // protected function defineSettings()
    // {
    //
    //     $settings['layouts']  = array(AttributeType::Mixed, 'default' => '');
    //
    //     return $settings;
    // }

    protected function getSettingsModel()
	{
		return new Layouts_FieldSettingsModel($this->model);
	}

    /**
     * Returns the field's input HTML.
     *
     * @param string $name
     * @param mixed  $value
     * @return string
     */
    public function getInputHtml($name, $value)
    {
        if (!$value)
        {
            $value = new Layouts_LayoutFieldModel();
        }

        $id = craft()->templates->formatInputId($name);
        $namespacedId = craft()->templates->namespaceInputId($id);
        $nameId = str_replace("-".$name,"",$namespacedId);
        $settings = $this->getSettings();


/* -- Include our Javascript & CSS */

        craft()->templates->includeCssResource('layouts/css/dist/layouts.css');
        craft()->templates->includeJsFile('https://cdn.polyfill.io/v2/polyfill.min.js?features=default,es6,fetch&flags=gated');
        craft()->templates->includeJsResource('layouts/js/dist/layouts.js');


        $jsonVars = array(
            'id' => $id,
            'name' => $name,
            'namespace' => $namespacedId,
            'nameId' => $nameId,
            'prefix' => craft()->templates->namespaceInputId("")
        );
        $jsonVars = json_encode($jsonVars);

        craft()->templates->includeJs("new LayoutsField('#{$namespacedId}-field .layout--element'," . $jsonVars . " );");


/* -- Variables to pass down to our rendered template */

        $variables = array(
            'id' => $id,
            'name' => $name,
            'namespaceId' => $namespacedId,
            'nameId' => $nameId,
            'layoutValue' => $value,
            'layouts' => $settings['layouts']
        );

        return craft()->templates->render('layouts/fields/Layouts_LayoutsFieldType.twig', $variables);
    }

    /**
     * Returns the field's settings HTML.
     *
     * @return string|null
     */
    public function getSettingsHtml()
    {
        craft()->templates->includeCssResource('layouts/css/dist/layouts.css');
        craft()->templates->includeJsFile('https://cdn.polyfill.io/v2/polyfill.min.js?features=default,es6,fetch&flags=gated');
        craft()->templates->includeJsResource('layouts/js/vendor/jquerySerializeObject.js');
        craft()->templates->includeJsResource('layouts/js/dist/layouts.js');

        craft()->templates->includeJs("new LayoutsSettings('".craft()->templates->namespaceInputId('layouts')."')");
        return craft()->templates->render('layouts/settings/Layouts_FieldTypeSettings', array(
            'defaultLayouts' => LayoutsHelper::defaultLayouts(),
            'settings' => $this->getSettings(),
            'id' => 'layouts'
        ));
    }

    /**
     * Returns the input value as it should be saved to the database.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValueFromPost($value)
    {
        $settings = $this->getSettings();
        $layouts = $settings['layouts'];

        foreach ($layouts as $layout)
        {
            if (strtolower($layout->handle) == strtolower($value['handle']))
            {
                $layoutModel = array();

                $layoutModel['id'] = $layout->id;
                $layoutModel['handle'] = $value['handle'];
                $layoutModel['icon'] = $layout->icon;
                $layoutModel['description'] = $layout->description;
                $layoutModel['entries'] = array_key_exists('entries', $value) ? $value['entries'] : [];  
                $layoutModel['sources'] = $layout->getSources();
                $layoutModel['entryTypes'] = $layout->getEntryTypes();
                $layoutModel['limit'] = $layout->getLimit();

                return $layoutModel;
            }
        }

        return '';
    }


    /**
     * Prepares the field's value for use.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValue($value)
    {
        if(!$value) return null;

        $value = new Layouts_LayoutFieldModel($value);
        return $value;
    }


    /**
	 * @inheritDoc ISavableComponentType::prepSettings()
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function prepSettings($settings)
	{
        if ($settings instanceof Layouts_FieldSettingsModel)
		{
			return $settings;
		}

        $layoutsSettings = new Layouts_FieldSettingsModel($this->model);
        $layouts = array();

        if (isset($settings))
        {
            foreach ($settings["layouts"] as $item)
            {
                $layout = json_decode($item,true);

                $layoutModel = new Layouts_LayoutsModel();
                $layoutModel->setAttribute('id', $layout['id']);
                $layoutModel->setAttribute('handle', $layout['handle']);
                $layoutModel->setAttribute('icon', $layout['icon']);
                $layoutModel->setAttribute('description', $layout['description']);
                $layoutModel->setAttribute('entries', $layout['entries']);

                $layouts[] = $layoutModel;

            }

            $layoutsSettings->layouts = $layouts;

        }

        return $layoutsSettings;
    }
}
