<?php
namespace Craft;

class Layouts_SettingsController extends BaseController
{
    protected $allowAnonymous = array('actionDefaultLayouts,actionLayouts,actionGetSectionEntryTypes');

    public function actionDefaultLayouts() {
        return craft()->layouts_settings->defaultLayouts();
    }

    public function actionLayouts() {
        return craft()->layouts_settings->availableLayouts();
    }

}
