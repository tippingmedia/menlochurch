const webpack = require('webpack');
const nodeEnv = process.env.NODE_ENV || 'production';

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass = new ExtractTextPlugin({
    filename: "[name].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    devtool: 'source-map',
    entry: {
        filename: './src/js/emailtemplates.js'
    },
    output: {
        filename: './dist/emailtemplates.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['babel-present-env']
            }
        }],
        rules: [{
            test: /\.scss$/,
            use: extractSass.extract({
                use: [{
                    loader: "css-loader"
                }, {
                    loader: "sass-loader",
                    options: {
                        includePaths: ["./src/scss/emailtemplates.scss"]
                    }
                }],
                // use style-loader in development
                fallback: "style-loader"
            })
        }]
    },
    plugins: [
        // uglify js
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false },
            output: { comments: false },
            sourceMap: true
        }),
        // env plugin
        new webpack.DefinePlugin({
            'proccess.env': { NODE_ENV: JSON.stringify(nodeEnv) }
        }),
        extractSass
    ]
}