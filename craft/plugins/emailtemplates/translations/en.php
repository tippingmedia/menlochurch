<?php
/**
 * EmailTemplates plugin for Craft CMS
 *
 * EmailTemplates Translation
 *
 * @author    Tipping Media 
 * @copyright Copyright (c) 2017 Tipping Media 
 * @link      http://tippingmedia.com
 * @package   EmailTemplates
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
