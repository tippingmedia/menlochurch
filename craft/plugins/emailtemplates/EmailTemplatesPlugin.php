<?php
/**
 * EmailTemplates plugin for Craft CMS
 *
 * Provide Contact Form plugin with email templates to use.

 *
 * @author    Tipping Media 
 * @copyright Copyright (c) 2017 Tipping Media 
 * @link      http://tippingmedia.com
 * @package   EmailTemplates
 * @since     1.0.0
 */

namespace Craft;

class EmailTemplatesPlugin extends BasePlugin
{
    public function init()
    {
        craft()->on('contactForm.beforeMessageCompile', function(ContactFormMessageEvent $event) {
            // The 'postedMessage' param is whatever $_POST['message'] was set to
            $postedMessage = $event->params['postedMessage'];
            
            // Take over the actual message compilation if you want. But make sure there's a posted message
            // in either string or array format so validation can run its course.
            if (is_array($postedMessage) && isset($postedMessage['handle']) && !empty($postedMessage['handle']))
            {
                
                $settings = $this->getSettings();
                $templateString = '';

                foreach ($settings->templates as $temp) {
                    if($temp['handle'] == $postedMessage['handle']) {
                        $templateString = $temp['template'];
                    }
                }

                $compiledMessage = craft()->templates->renderString(StringHelper::parseMarkdown($templateString), $postedMessage);

                $event->message = $compiledMessage;
                $event->htmlMessage = $compiledMessage;
                
            }
        });
    }

    /**
     * Returns the user-facing name.
     *
     * @return mixed
     */
    public function getName()
    {
         return Craft::t('Email Templates');
    }

    /**
     * Plugins can have descriptions of themselves displayed on the Plugins page by adding a getDescription() method
     * on the primary plugin class:
     *
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t('Provide Contact Form Plugin with email templates.');
    }

    /**
     * Plugins can now take part in Craft’s update notifications, and display release notes on the Updates page, by
     * providing a JSON feed that describes new releases, and adding a getReleaseFeedUrl() method on the primary
     * plugin class.
     *
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return '???';
    }

    /**
     * Returns the version number.
     *
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * As of Craft 2.5, Craft no longer takes the whole site down every time a plugin’s version number changes, in
     * case there are any new migrations that need to be run. Instead plugins must explicitly tell Craft that they
     * have new migrations by returning a new (higher) schema version number with a getSchemaVersion() method on
     * their primary plugin class:
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return '1.0.0';
    }

    /**
     * Returns the developer’s name.
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Tipping Media LLC';
    }

    /**
     * Returns the developer’s website URL.
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'http://tippingmedia.com';
    }

    /**
     * Returns whether the plugin should get its own tab in the CP header.
     *
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }


    public function getSettingsHtml()
    {
        return craft()->templates->render('emailtemplates/EmailTemplates_Settings', [
            'settings' => $this->getSettings()
        ]);
    }

    public function defineSettings()
    {
        return [
            'templates' => AttributeType::Mixed
        ];
    }

}