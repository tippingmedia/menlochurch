<?php
namespace Craft;
/**
 * LocaleHide Plugin
 * @author Tipping Media
 */
class LocaleHidePlugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('Locale Hide');
    }
    
    public function getVersion()
    {
        return '0.1.1';
    }

    public function getSchemaVersion()
    {
        return '0.1.1';
    }

    public function getDescription()
    {
        return 'A plugin to hide the locale list.';
    }

    public function getDocumentationUrl()
    {
        return 'http://tippingmedia.com';
    }

    public function getDeveloper()
    {
        return 'Tipping Media';
    }

    public function getDeveloperUrl()
    {
        return 'http://tippingmedia.com';
    }

    // public function getIconPath()
    // {
    //     return craft()->path->getPluginsPath().'/resources/img/linklist.svg';
    // }

    // public function hasCpSection()
    // {
    //     return false;
    // }

}
