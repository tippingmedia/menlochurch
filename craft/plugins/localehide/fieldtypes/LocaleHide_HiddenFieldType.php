<?php

/**
 * LocaleHide by TippingMedia
 *
 * @package   LinkList
 * @author    Adam Randlett
 * @copyright Copyright (c) 2015, Tipping Media
 */

namespace Craft;

class LocaleHide_HiddenFieldType extends BaseFieldType
{

    /**
     * Block type name
     */
    public function getName()
    {
        return Craft::t('Hide Locales');
    }


    /**
     * Save it
     */
    public function defineContentAttribute()
    {
        return false;
    }
    
        

    /**
     * Returns the field's input HTML.
     *
     * @param string $name
     * @param mixed  $value
     * @return string
     */
    public function getInputHtml($name,$value)
    {
        return craft()->templates->render('localehide/input', array(
            'name' => $name,
        ));
    }


    

 /**
     * Returns the input value as it should be saved to the database.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValueFromPost($value)
    {
       return json_encode($value); 
    }



    public function prepValue($value)
    { 
        return $value;
    }




}
