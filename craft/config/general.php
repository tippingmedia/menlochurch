<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    // All environments
    '*' => array(
        'omitScriptNameInUrls' => true,
        'usePathInfo' => true,
        'cacheDuration' => false,
        'phpMaxMemoryLimit' => '1512M',
        'appId'    => $_SERVER['SERVER_NAME'],
        'maxUploadFileSize' => 80000000,
        'backupDbOnUpdate' => false,
        'errorTemplatePrefix' => "_errors/",
        'imageDriver' => 'imagick',
        'siteUrl' => array(
            'mlo' => 'https://menlo.church',
            'mlo_park' =>  'https://menlo.church/menlopark/',
            'mlo_sm' =>  'https://menlo.church/sanmateo/',
            'mlo_mv' =>  'https://menlo.church/mountainview/',
            'mlo_sj' =>  'https://menlo.church/sanjose/',
            'mlo_st' =>  'https://menlo.church/saratoga/',
            'mlo_sc' =>  'https://menlo.church/southcity/',
            'mlo_on' =>  'https://menlo.church/online/',
        ),
        'craftEnv' => CRAFT_ENVIRONMENT,
        // Set the environmental variables
        'environmentVariables' => array(
            'baseUrl'  => 'https://menlo.church/',
            'basePath' => getenv('CRAFTENV_BASE_PATH'),
        ),
        'generateTransformsBeforePageLoad' => true,
        'defaultSearchTermOptions' => array(
            'subLeft' => true,
            'subRight' => true,
        ),
    ),
    // Live (production) environment
    'live'  => array(
        'isSystemOn' => true,
        'devMode' => true,
        'enableTemplateCaching' => true,
        'allowAutoUpdates' => false,
        'generateTransformsBeforePageLoad' => true,
        'siteUrl' => array(
            'mlo' => 'https://menlo.church',
            'mlo_park' =>  'https://menlo.church/menlopark/',
            'mlo_sm' =>  'https://menlo.church/sanmateo/',
            'mlo_mv' =>  'https://menlo.church/mountainview/',
            'mlo_sj' =>  'https://menlo.church/sanjose/',
            'mlo_st' =>  'https://menlo.church/saratoga/',
            'mlo_sc' =>  'https://menlo.church/southcity/',
            'mlo_on' =>  'https://menlo.church/online/',
        ),
    ),
    // Staging (pre-production) environment
    'staging'  => array(
        'isSystemOn' => false,
        'devMode' => false,
        'enableTemplateCaching' => true,
        'allowAutoUpdates' => false,
        'siteUrl' => array(
            'mlo' => getenv('CRAFTENV_SITE_URL'),
            'mlo_park' => getenv('CRAFTENV_SITE_URL') . 'menlopark/',
            'mlo_sm' => getenv('CRAFTENV_SITE_URL') . 'sanmateo/',
            'mlo_mv' => getenv('CRAFTENV_SITE_URL') . 'mountainview/',
            'mlo_sj' => getenv('CRAFTENV_SITE_URL') . 'sanjose/',
            'mlo_st' => getenv('CRAFTENV_SITE_URL') . 'saratoga/',
            'mlo_sc' => getenv('CRAFTENV_SITE_URL') . 'southcity/',
            'mlo_on' => getenv('CRAFTENV_SITE_URL') . 'online/',
        ),
    ),
    // Local (development) environment
    'local'  => array(
        'isSystemOn' => true,
        'devMode' => true,
        'usePathInfo' => true,
        'imageDriver' => 'gd',
        'enableTemplateCaching' => false,
        'allowAutoUpdates' => true,
        'siteUrl' => array(
            'mlo' => "https://menlochurch.test/",
            'mlo_park' =>  'https://menlochurch.test/menlopark/',
            'mlo_sm' =>  'https://menlochurch.test/sanmateo/',
            'mlo_mv' =>  'https://menlochurch.test/mountainview/',
            'mlo_sj' =>  'https://menlochurch.test/sanjose/',
            'mlo_st' =>  'https://menlochurch.test/saratoga/',
            'mlo_sc' =>  'https://menlochurch.test/southcity/',
            'mlo_on' => getenv('CRAFTENV_SITE_URL') . 'online/',
        ),
        'environmentVariables' => array(
            'baseUrl'  => "https://menlochurch.test/",
            'basePath' => getenv('CRAFTENV_BASE_PATH'),
        ),
    ),
);
