<?php

return array(
	//'imagerUrl' => '//s3.amazonaws.com/media.menlo.church/imager/',
	'imagerUrl' => getenv('CRAFTENV_CLOUDFRONT_URL') . '/imager/',
    'jpegoptimEnabled' => false,
    'optipngEnabled' => false,
	'awsEnabled' => true,
	'awsAccessKey' => getenv('CRAFTENV_S3_SECRET_KEY'),
	'awsSecretAccessKey' => getenv('CRAFTENV_S3_ACCESS_KEY'),
	'awsBucket' => 'media.menlo.church',
	'awsFolder' => 'imager',
  	'clearKey' => 'z8mgl32islei'
);
