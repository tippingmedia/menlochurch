<?php
namespace Craft;

return [
    'defaults' => [
        'cache' => 'PT1H'
    ],
    'endpoints' => [

        'api/messages.json' => [
            'elementType' => 'Entry',
            'elementsPerPage' => 6,
            'pageParam' => 'pg',
            'criteria' => [
                'section' => 'messages', 
                'order' => 'messageDate DESC',
                'with' => ['messageImage','messageTopic','series','messageSpeaker']
                ],
            'transformer' => function(EntryModel $entry) {
                $imageUrl = "";
                if (isset($entry->messageImage[0])) {
                    $srcImage = $entry->messageImage[0];
                    if ($srcImage)
                    {
                        craft()->config->set('generateTransformsBeforePageLoad', true);
                        $imageUrl = $srcImage->getUrl([
                            'width' => 720, 
                            'height' => 480, 
                            'mode' => 'crop', 
                            'quality' => 75, 
                            'position' => 'center-center'
                        ]);
                    }
                }
                if(isset($entry->messageVideo[0])){
                    $imageUrl = $entry->messageVideo->getThumbnail(571);
                }

                $seriesImageUrl = "";
                if (isset($entry->series[0])) {
                    if (isset($entry->series[0]->seriesImage[0])) {
                        $srcImage = $entry->series[0]->seriesImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $seriesImageUrl = $srcImage->getUrl([
                                'width' => 120, 
                                'height' => 120, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                }

                $topics = [];
                foreach ($entry->messageTopic as $cat)
                    $topics[] = $cat->title;

                return [
                    'title' => $entry->title,
                    'id' => $entry->id,
                    'url' => $entry->url,
                    'locale' => $entry->locale,
                    'type' => $entry->type->handle,
                    'video' => isset($entry->messageVideo[0]) ? $entry->messageVideo->getEmbed(['width' =>571, 'height' =>321]) : null,
                    'messageImageUrl' => $imageUrl,
                    'messageSummary' => $entry->messageSummary,
                    'messageTopics' => $topics,
                    'messageDate' => $entry->messageDate->format('M d, Y'),
                    'messageSpeaker' => isset($entry->messageSpeaker[0]) ? [
                        'title' => $entry->messageSpeaker[0]->title
                    ] : [],
                    'series' => isset($entry->series[0]) ? [
                        'title' => $entry->series[0]->title,
                        'image' => $seriesImageUrl
                    ] : false
                    
                ];
            },
        ],
        'api/messages/<elementId:\d+>.json' => function ($elementId) {
            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 6,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'messages', 
                    'relatedTo' => ['and',['targetElement' => $elementId] ], 
                    'order' => 'messageDate DESC',
                    'with' => ['messageImage','messageTopic','messageSpeaker']
                    ],
                'transformer' => function(EntryModel $entry) {
                    $imageUrl = "";
                    if (isset($entry->messageImage[0])) {
                        $srcImage = $entry->messageImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 720, 
                                'height' => 480, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                    if(isset($entry->messageVideo[0])){
                        $imageUrl = $entry->messageVideo->getThumbnail(571);
                    }

                    $seriesImageUrl = "";
                    if (isset($entry->series[0])) {
                        if (isset($entry->series[0]->seriesImage[0])) {
                            $srcImage = $entry->series[0]->seriesImage[0];
                            if ($srcImage)
                            {
                                craft()->config->set('generateTransformsBeforePageLoad', true);
                                $seriesImageUrl = $srcImage->getUrl([
                                    'width' => 120, 
                                    'height' => 120, 
                                    'mode' => 'crop', 
                                    'quality' => 75, 
                                    'position' => 'center-center'
                                ]);
                            }
                        }
                    }

                    $topics = [];
                    foreach ($entry->messageTopic as $cat)
                        $topics[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'url' => $entry->url,
                        'locale' => $entry->locale,
                        'type' => $entry->type->handle,
                        'video' => isset($entry->messageVideo[0]) ? $entry->messageVideo->getEmbed(['width' =>571, 'height' =>321]) : null,
                        'messageImageUrl' => $imageUrl,
                        'messageSummary' => $entry->messageSummary,
                        'messageTopics' => $topics,
                        'messageDate' => $entry->messageDate->format('M d, Y'),
                        'messageSpeaker' => isset($entry->messageSpeaker[0]) ? [
                            'title' => $entry->messageSpeaker[0]->title
                        ] : [],
                        'series' => isset($entry->series[0]) ? [
                            'title' => $entry->series[0]->title,
                            'image' => $seriesImageUrl
                        ] : false
                        
                    ];
                },
            ];
        },
        'api/messages/year/<year:\d+>.json' => function ($year) {
            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 6,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'messages', 
                    'messageDate' => function() {
                        $sd = new DateTime($year . '-01-01');
                        $ed = new DateTime($year . '-12-31');
                        return ['and',">= ". $sd, "<= ".$ed ];
                    } ,
                    'order' => 'messageDate DESC',
                    'with' => ['messageImage','messageTopic','messageSpeaker']
                    ],
                'transformer' => function(EntryModel $entry) {
                    $imageUrl = "";
                    if (isset($entry->messageImage[0])) {
                        $srcImage = $entry->messageImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 720, 
                                'height' => 480, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                    if(isset($entry->messageVideo[0])){
                        $imageUrl = $entry->messageVideo->getThumbnail(571);
                    }

                    $seriesImageUrl = "";
                    if (isset($entry->series[0])) {
                        if (isset($entry->series[0]->seriesImage[0])) {
                            $srcImage = $entry->series[0]->seriesImage[0];
                            if ($srcImage)
                            {
                                craft()->config->set('generateTransformsBeforePageLoad', true);
                                $seriesImageUrl = $srcImage->getUrl([
                                    'width' => 120, 
                                    'height' => 120, 
                                    'mode' => 'crop', 
                                    'quality' => 75, 
                                    'position' => 'center-center'
                                ]);
                            }
                        }
                    }

                    $topics = [];
                    foreach ($entry->messageTopic as $cat)
                        $topics[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'url' => $entry->url,
                        'locale' => $entry->locale,
                        'type' => $entry->type->handle,
                        'video' => isset($entry->messageVideo[0]) ? $entry->messageVideo->getEmbed(['width' =>571, 'height' =>321]) : null,
                        'messageImageUrl' => $imageUrl,
                        'messageSummary' => $entry->messageSummary,
                        'messageTopics' => $topics,
                        'messageDate' => $entry->messageDate->format('M d, Y'),
                        'messageSpeaker' => isset($entry->messageSpeaker[0]) ? [
                            'title' => $entry->messageSpeaker[0]->title
                        ] : [],
                        'series' => isset($entry->series[0]) ? [
                            'title' => $entry->series[0]->title,
                            'image' => $seriesImageUrl
                        ] : false
                        
                    ];
                },
            ];
        },
        'api/messages/search/<query:(.*)>.json' => function ($query) {
            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 6,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'messages', 
                    'search' => '*' . $query . '*', 
                    'order' => 'messageDate DESC',
                    'with' => ['messageTopic']
                ],
                'transformer' => function(EntryModel $entry) {
                    $imageUrl = "";
                    if (isset($entry->messageImage[0])) {
                        $srcImage = $entry->messageImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 720, 
                                'height' => 480, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                    if(isset($entry->messageVideo[0])){
                        $imageUrl = $entry->messageVideo->getThumbnail(571);
                    }

                    $seriesImageUrl = "";
                    if (isset($entry->series[0])) {
                        if (isset($entry->series[0]->seriesImage[0])) {
                            $srcImage = $entry->series[0]->seriesImage[0];
                            if ($srcImage)
                            {
                                craft()->config->set('generateTransformsBeforePageLoad', true);
                                $seriesImageUrl = $srcImage->getUrl([
                                    'width' => 120, 
                                    'height' => 120, 
                                    'mode' => 'crop', 
                                    'quality' => 75, 
                                    'position' => 'center-center'
                                ]);
                            }
                        }
                    }

                    $topics = [];
                    foreach ($entry->messageTopic as $cat)
                        $topics[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'url' => $entry->url,
                        'locale' => $entry->locale,
                        'type' => $entry->type->handle,
                        'video' => isset($entry->messageVideo[0]) ? $entry->messageVideo->getEmbed(['width' =>571, 'height' =>321]) : null,
                        'messageImageUrl' => $imageUrl,
                        'messageSummary' => $entry->messageSummary,
                        'messageTopics' => $topics,
                        'messageDate' => $entry->messageDate->format('M d, Y'),
                        'messageSpeaker' => isset($entry->messageSpeaker[0]) ? [
                            'title' => $entry->messageSpeaker[0]->title
                        ] : [],
                        'series' => isset($entry->series[0]) ? [
                            'title' => $entry->series[0]->title,
                            'image' => $seriesImageUrl
                        ] : false
                        
                    ];
                }
            ];
        },

        'api/series.json' => [
            'elementType' => 'Entry',
            'elementsPerPage' => 6,
            'pageParam' => 'pg',
            'criteria' => [
                'section' => 'messageSeries', 
                'with' => ['seriesImage']
                ],
            'transformer' => function(EntryModel $entry) {
                $imageUrl = "";
                if (isset($entry->seriesImage[0])) {
                    $srcImage = $entry->seriesImage[0];
                    if ($srcImage)
                    {
                        craft()->config->set('generateTransformsBeforePageLoad', true);
                        $imageUrl = $srcImage->getUrl([
                            'width' => 471, 
                            'height' => 471, 
                            'mode' => 'crop', 
                            'quality' => 75, 
                            'position' => 'center-center'
                        ]);
                    }
                }
                return [
                    'title' => $entry->title,
                    'id' => $entry->id,
                    'url' => $entry->url,
                    'locale' => $entry->locale,
                    'type' => $entry->type->handle,
                    'video' => isset($entry->seriesPromo[0]) ? $entry->seriesPromo->getEmbed(['width' =>571, 'height' =>321]): null,
                    'imageUrl' => $imageUrl,
                    'description' => $entry->seriesDescription,
                    'current' => $entry->seriesCurrent
                ];
            },
        ],

        'api/serve.json' => [
            'elementType' => 'Entry',
            'paginate' => false,
            'criteria' => [
                'section' => 'serve', 
                'limit' => '25',
                'order' => 'serveDeadline DESC',
                'servePartnership' => '!=1',
                'with' => ['serveImage','serveTeams']
            ],
            'transformer' => function(EntryModel $entry) {
                $imageUrl = "";
                if (isset($entry->serveImage[0])) {
                    $srcImage = $entry->serveImage[0];
                    if ($srcImage)
                    {
                        craft()->config->set('generateTransformsBeforePageLoad', true);
                        $imageUrl = $srcImage->getUrl([
                            'width' => 360, 
                            'height' => 360, 
                            'mode' => 'crop', 
                            'quality' => 75, 
                            'position' => 'center-center'
                        ]);
                    }
                }

                $teams = [];
                foreach ($entry->serveTeams as $cat)
                    $teams[] = $cat->title;

                return [
                    'title' => $entry->title,
                    'id' => $entry->id,
                    'type' => $entry->type->handle,
                    'locale' => $entry->locale,
                    'imageUrl' => $imageUrl,
                    'serveOverview' => $entry->serveOverview,
                    'teams' => $teams,
                    'meetingSchedule' => $entry->serveMeetingSchedule,
                    'location' => isset($entry->serveLocation[0]) ? [
                        'title' => $entry->serveLocation[0]->title,
                        'mapUrl' => $entry->serveLocation[0]->getMapUrl(),
                        'specific' => $entry->serveSpecificLocation
                    ] : [],
                    'serveDeadline' => $entry->serveDeadline ? $entry->serveDeadline->format('c') : ''
                ];
            }
        ],
        'api/serve/featured.json' => [
            'elementType' => 'Entry',
            'elementsPerPage' => 6,
            'pageParam' => 'pg',
            'criteria' => [
                'section' => 'serve', 
                'serveFeatured' => 1,
                'order' => 'serveDeadline DESC',
                'with' => ['serveImage','serveTeams']
            ],
            'transformer' => function(EntryModel $entry) {
                $imageUrl = "";
                if (isset($entry->serveImage[0])) {
                    $srcImage = $entry->serveImage[0];
                    if ($srcImage)
                    {
                        craft()->config->set('generateTransformsBeforePageLoad', true);
                        $imageUrl = $srcImage->getUrl([
                            'width' => 360, 
                            'height' => 360, 
                            'mode' => 'crop', 
                            'quality' => 75, 
                            'position' => 'center-center'
                        ]);
                    }
                }

                $teams = [];
                foreach ($entry->serveTeams as $cat)
                    $teams[] = $cat->title;

                return [
                    'title' => $entry->title,
                    'id' => $entry->id,
                    'type' => $entry->type->handle,
                    'locale' => $entry->locale,
                    'imageUrl' => $imageUrl,
                    'serveOverview' => $entry->serveOverview,
                    'teams' => $teams,
                    'meetingSchedule' => $entry->serveMeetingSchedule,
                    'location' => isset($entry->serveLocation[0]) ? [
                        'title' => $entry->serveLocation[0]->title,
                        'mapUrl' => $entry->serveLocation[0]->getMapUrl(),
                        'specific' => $entry->serveSpecificLocation
                    ] : [],
                    'serveDeadline' => $entry->serveDeadline ? $entry->serveDeadline->format('c') : ''
                ];
            }
        ],
        'api/serve/<elementId:\d+(?:[,]\d*)*>.json' => function ($elementId) {

            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 6,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'serve', 
                    'relatedTo' => ['and',['targetElement' => $elementId] ], 
                    'order' => 'serveDeadline DESC',
                    'servePartnership' => '!=1',
                    'with' => ['serveImage','serveTeams']
                ],
                'transformer' => function(EntryModel $entry) {
                    $imageUrl = "";
                    if (isset($entry->serveImage[0])) {
                        $srcImage = $entry->serveImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 360, 
                                'height' => 360, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }

                    $teams = [];
                    foreach ($entry->serveTeams as $cat)
                        $teams[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'type' => $entry->type->handle,
                        'locale' => $entry->locale,
                        'imageUrl' => $imageUrl,
                        'serveOverview' => $entry->serveOverview,
                        'teams' => $teams,
                        'meetingSchedule' => $entry->serveMeetingSchedule,
                        'location' => isset($entry->serveLocation[0]) ? [
                            'title' => $entry->serveLocation[0]->title,
                            'mapUrl' => $entry->serveLocation[0]->getMapUrl(),
                            'specific' => $entry->serveSpecificLocation
                        ] : [],
                        'serveDeadline' => $entry->serveDeadline ? $entry->serveDeadline->format('c') : ''
                    ];
                },
            ];
        },
        'api/serve/search/<query:(.*)>.json' => function ($query) {
            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 50,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'serve', 
                    'search' => '*' . $query . '*', 
                    'order' => 'postDate DESC',
                    'with' => ['serveImage','serveTeams'],
                    'servePartnership' => '!=1'
                ],
                'transformer' => function(EntryModel $entry) {
                    $imageUrl = "";
                    if (isset($entry->serveImage[0])) {
                        $srcImage = $entry->serveImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 360, 
                                'height' => 360, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }

                    $teams = [];
                    foreach ($entry->serveTeams as $cat)
                        $teams[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'type' => $entry->type->handle,
                        'locale' => $entry->locale,
                        'imageUrl' => $imageUrl,
                        'serveOverview' => $entry->serveOverview,
                        'teams' => $teams,
                        'meetingSchedule' => $entry->serveMeetingSchedule,
                        'location' => isset($entry->serveLocation[0]) ? [
                            'title' => $entry->serveLocation[0]->title,
                            'mapUrl' => $entry->serveLocation[0]->getMapUrl(),
                            'specific' => $entry->serveSpecificLocation
                        ] : [],
                        'serveDeadline' => $entry->serveDeadline ? $entry->serveDeadline->format('c') : ''
                    ];
                }
            ];
        },


        'api/monthEvents/<groups:\d{1,3}(?:[,]\d*)*>/<campuses:\w{1,8}(?:[,]\w*)*>/<start:((\d{4})-(?:0?[1-9]|1[012])-(?:0?[1-9]|[12][0-9]|3[01]){1,2})>/<end:((\d{4})-(?:0?[1-9]|1[012])-(?:0?[1-9]|[12][0-9]|3[01]){1,2})>.json' => function ($groups,$campuses,$start,$end) {
            return [
                'elementType' => 'Venti_Event',
                'paginate' => false,
                'elementsPerPage' => null,
                'criteria' => [
                    'groupId' => $groups, 
                    'between' => [$start.' 00:00:00',$end.' 23:59:59'],
                    'locale' => $campuses
                ],
                'transformer' => function(Venti_EventModel $event) {
                    $imageUrl = "";
                    if (isset($event->eventImage[0])) {
                        $srcImage = $event->eventImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 100, 
                                'height' => 100, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                    return [ 
                        'title' => $event->title,
                        'eid' => $event->eid,
                        'id' => $event->cid,
                        'locale' => $event->locale,
                        'startDate' => $event->startDate->format('c'),
                        'endDate' => $event->endDate->format('c'),
                        'allDay' => $event->allDay,
                        'summary' => $event->summary,
                        'repeat' => $event->repeat,
                        'isrepeat' => $event->isrepeat,
                        'eventImage' => $imageUrl,
                        'color' => $event->color,
                        'group' => $event->group->name,
                        'groupId' => $event->group->id,
                        'register' => is_object($event->registration) ? $event->registration->url : '',
                        'location' => isset($event->location[0]) ?? [
                            'title' => $event->location[0]->title,
                            'mapUrl' => $event->location[0]->getMapUrl()
                        ]
                    ];
                }
            ];
        },
        'api/searchEvents/<campuses:\w{1,8}(?:[,]\w*)*>/<query:(.*)>.json' => function ($campuses,$query) {
            return [
                'elementType' => 'Venti_Event',
                'paginate' => false,
                'elementsPerPage' => null,
                'criteria' => [
                    'endDate' => ['and','>= '.new DateTime()],
                    'locale' => $campuses,
                    'search' => '*' . $query . '*',
                    'limit' => 25
                ],
                'transformer' => function(Venti_EventModel $event) {
                    $imageUrl = "";
                    if (isset($event->eventImage[0])) {
                        $srcImage = $event->eventImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 100, 
                                'height' => 100, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                    return [ 
                        'title' => $event->title,
                        'eid' => $event->eid,
                        'id' => $event->cid,
                        'locale' => $event->locale,
                        'startDate' => $event->startDate->format('c'),
                        'endDate' => $event->endDate->format('c'),
                        'allDay' => $event->allDay,
                        'summary' => $event->summary,
                        'repeat' => $event->repeat,
                        'isrepeat' => $event->isrepeat,
                        'eventImage' => $imageUrl,
                        'color' => $event->color,
                        'group' => $event->group->name,
                        'groupId' => $event->group->id,
                        'register' => is_object($event->registration) ? $event->registration->url : '',
                        'location' => isset($event->location[0]) ?? [
                            'title' => $event->location[0]->title,
                            'mapUrl' => $event->location[0]->getMapUrl()
                        ]
                    ];
                }
            ];
        },
        'api/featuredEvents/<groups:\d{1,3}(?:[,]\d*)*>/<campuses:\w{1,8}(?:[,]\w*)*>/<start:((\d{4})-(?:0?[1-9]|1[012])-(?:0?[1-9]|[12][0-9]|3[01]){1,2})>/<end:((\d{4})-(?:0?[1-9]|1[012])-(?:0?[1-9]|[12][0-9]|3[01]){1,2})>.json' => function ($groups,$campuses,$start,$end) {
            return [
                'elementType' => 'Venti_Event',
                'paginate' => false,
                'elementsPerPage' => null,
                'criteria' => [
                    'groupId' => $groups, 
                    'featuredEvent' => 1, 
                    //'startDate' => ['and','>= '.new DateTime()],
                    'between' => [$start.' 00:00:00',$end.' 23:59:59'],
                    'locale' => $campuses
                ],
                'transformer' => function(Venti_EventModel $event) {
                    $imageUrl = "";
                    if (isset($event->eventImage[0])) {
                        $srcImage = $event->eventImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 360, 
                                'height' => 360, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }
                    
                    return [ 
                        'title' => $event->title,
                        'eid' => $event->eid,
                        'id' => $event->cid,
                        'locale' => $event->locale,
                        'startDate' => $event->startDate->format('c'),
                        'endDate' => $event->endDate->format('c'),
                        'eventImage' => $imageUrl,
                        'allDay' => $event->allDay,
                        'summary' => $event->summary,
                        'repeat' => $event->repeat,
                        'isrepeat' => $event->isrepeat,
                        'color' => $event->color,
                        'group' => $event->group->name,
                        'groupId' => $event->group->id,
                        'specificLocation' => $event->specificLocation,
                        'location' => isset($event->location[0]) ?? [
                            'title' => $event->location[0]->title,
                            'mapUrl' => $event->location[0]->getMapUrl()
                        ]
                    ];
                }
            ];
        },

        'api/staff/<elementId:\d+>.json' => function ($elementId) {

            $targets = ['and'];
            array_push($targets,["targetElement" => 1254]);
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'people';
            $criteria->relatedTo = $targets;
            $criteria->limit = null;
            $entryIds = "and,not " . implode(",not ", $criteria->ids());

            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 8,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'people', 
                    'relatedTo' => ['and',['targetElement' => $elementId] ], 
                    'with' => ['profileImage'],
                    'id' => explode(",", $entryIds),
                    ],
                'transformer' => function(EntryModel $entry) {
                    craft()->config->set('generateTransformsBeforePageLoad', true);
                    $imageUrl = "";
                    if (isset($entry->profileImage[0])) {
                        $srcImage = $entry->profileImage[0];
                        if ($srcImage)
                        {
                            $imageUrl = $srcImage->getUrl([
                                'width' => 387, 
                                'height' => 581, 
                                'mode' => 'crop', 
                                'quality' => 80, 
                                'position' => 'center-center'
                            ]);
                        }
                    }

                    $name = $entry->firstName . " " . $entry->lastName;
                    $expr = '/(?<=\s|^)[a-z]/i';
                    preg_match_all($expr, $name, $matches);
                    $initials = implode('', $matches[0]);
                    $initials = strtoupper($initials);

                    $social = [];
                    if (isset($entry->pSocialLinks[0])) {
                        foreach ($entry->pSocialLinks as $item){
                            $social[] = [
                                'serviceUrl' => $item->serviceUrl,
                                'service' => [
                                    'label' => $item->service->label,
                                    'value' => $item->service->value
                                ]
                            ];
                        };
                    }

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'locale' => $entry->locale,
                        'type' => 'person',
                        'profileImageUrl' => $imageUrl,
                        'professionalTitle' => $entry->professionalTitle,
                        'social' => $social,
                        'initials' => $initials
                    ];
                }
            ];
        },
        'api/staff/search/<query:(.*)>.json' => function ($query) {

            $targets = ['and'];
            array_push($targets,["targetElement" => 1254]);
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'people';
            $criteria->relatedTo = $targets;
            $criteria->limit = null;
            $entryIds = "and,not " . implode(",not ", $criteria->ids());

            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 12,
                'pageParam' => 'pg',
                'criteria' => [
                    'section' => 'people', 
                    'search' => '*' . $query . '*', 
                    'with' => ['profileImage'],
                    'id' => explode(",", $entryIds),
                    'order' => 'score'
                ],
                'transformer' => function(EntryModel $entry) {
                    craft()->config->set('generateTransformsBeforePageLoad', true);
                    $imageUrl = "";
                    if (isset($entry->profileImage[0])) {
                        $srcImage = $entry->profileImage[0];
                        if ($srcImage)
                        {
                            $imageUrl = $srcImage->getUrl([
                                'width' => 387, 
                                'height' => 581, 
                                'mode' => 'crop', 
                                'quality' => 80, 
                                'position' => 'center-center'
                            ]);
                        }
                    }

                    $name = $entry->firstName . " " . $entry->lastName;
                    $expr = '/(?<=\s|^)[a-z]/i';
                    preg_match_all($expr, $name, $matches);
                    $initials = implode('', $matches[0]);
                    $initials = strtoupper($initials);

                    $social = [];
                    if (isset($entry->pSocialLinks[0])) {
                        foreach ($entry->pSocialLinks as $item){
                            $social[] = [
                                'serviceUrl' => $item->serviceUrl,
                                'service' => [
                                    'label' => $item->service->label,
                                    'value' => $item->service->value
                                ]
                            ];
                        };
                    }
                    

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'locale' => $entry->locale,
                        'type' => 'person',
                        'profileImageUrl' => $imageUrl,
                        'professionalTitle' => $entry->professionalTitle,
                        'social' => $social,
                        'initials' => $initials
                    ];
                }
            ];
        },
        'api/campuses.json' => [
            'elementType' => 'Entry',
            'criteria' => [
                'section' => 'campuses',
                'with' => ['campusImage','socialLinks'],
                //'id' => ['and','NOT ' . 619],
                'order' => 'title ASC',
                'locale' => null
            ],
            'paginate' => false,
            'transformer' => function(EntryModel $entry) {
                $campus = [
                    'id' => $entry->id,
                    'name' => $entry->title,
                    'slug' => $entry->slug,
                    'locale' => $entry->locale,
                    'times' => $entry->worshipTimes,
                    'phone' => $entry->campusPhoneNumber,
                    'photo' => '',
                    'lat' => '',
                    'lng' => '',
                    'addr' => ''
                ];
                $campusUrls = [
                    'mlo' => '',
                    'mlo_park' => 'menlopark',
                    'mlo_mv' => 'mountainview',
                    'mlo_sj' => 'sanjose',
                    'mlo_sm' => 'sanmateo',
                    'mlo_st' => 'saratoga',
                    'mlo_sc' => 'southcity',
                    'mlo_on' => 'online'
                ];

                $baseurl = craft()->config->get('environmentVariables')['baseUrl'];
                $campus['url'] = $baseurl . $campusUrls[$entry->slug] .'/';

                $imageUrl = "";
                if (isset($entry->campusImage[0])) {
                    $srcImage = $entry->campusImage[0];
                    if ($srcImage)
                    {
                        craft()->config->set('generateTransformsBeforePageLoad', true);
                        $imageUrl = $srcImage->getUrl([
                            'width' => 320, 
                            'height' => 214, 
                            'mode' => 'crop', 
                            'quality' => 75, 
                            'position' => 'center-center'
                        ]);

                        $campus['photo'] = $imageUrl;
                    }
                }

                if(isset($entry->location[0])) {
                    $cl = $entry->location[0];

                    $campus['lat'] = $cl->latitude;
                    $campus['lng'] = $cl->longitude;
                    $campus['addr'] = $cl->address .'<br>'. $cl->city .', '. $cl->state .' '. $cl->zipCode;
                    $campus['mapUrl'] = $cl->getMapUrl();
                } 

                return $campus;
            }
        ],
        'api/stories.json' => [
            'elementType' => 'Entry',
            'elementsPerPage' => 6,
            'pageParam' => 'pg',
            'criteria' => [
                'section' => 'stories', 
                'order' => 'postDate DESC',
                'with' => ['postImage']
                ],
            'transformer' => function(EntryModel $entry) {
                $imageUrl = "";
                if (isset($entry->postImage[0])) {
                    $srcImage = $entry->postImage[0];
                    if ($srcImage)
                    {
                        craft()->config->set('generateTransformsBeforePageLoad', true);
                        $imageUrl = $srcImage->getUrl([
                            'width' => 720, 
                            'height' => 480, 
                            'mode' => 'crop', 
                            'quality' => 75, 
                            'position' => 'center-center'
                        ]);
                    }
                }

                return [
                    'title' => $entry->title,
                    'url' => $entry->url,
                    'type' => $entry->type->handle,
                    'locale' => $entry->locale,
                    'postImageUrl' => $imageUrl,
                    'postSummary' => $entry->postSummary,
                    'postDate' => $entry->postDate->format('M d, Y'),
                    'postSubHeading' => $entry->postSubHeading
                ];
            },
        ],



        'api/groups.json' => [
            'elementType' => 'Entry',
            'paginate' => false,
            'criteria' => [
                'section' => 'groups', 
                'limit' => null,
                'order' => 'postDate DESC',
                'with' => ['groupCategory','groupArea','groupLocation']
            ],
            'transformer' => function(EntryModel $entry) {
                
                $category = [];
                $categoryIds = [];
                foreach ($entry->groupCategory as $cat){
                    $category[] = $cat->title;
                    $categoryIds[] = $cat->id;
                }

                $areaIds = [];
                foreach ($entry->groupArea as $cat){
                    $areaIds[] = $cat->id;
                }
                
                $interestIds = [];
                foreach ($entry->groupInterests as $cat){
                    $interestIds[] = $cat->id;
                }

                return [
                    'title' => $entry->title,
                    'id' => $entry->id,
                    'locale' => $entry->locale,
                    'groupBegins' => $entry->groupBegins ? $entry->groupBegins->format('M d, Y') : '',
                    'groupHideBegins' => $entry->groupHideBegins,
                    'groupMeetingSchedule' => $entry->groupMeetingSchedule,
                    'childcare' => $entry->kidsWelcome,
                    'groupCategory' => $category,
                    'groupArea' => isset($entry->groupArea[0]) ? $entry->groupArea[0]->title : '',
                    'location' => isset($entry->groupLocation[0]) ? [
                        'title' => $entry->groupLocation[0]->title,
                        'mapUrl' => "http://maps.google.com/?q=". $entry->groupLocation[0]->fullAddress(),
                        'specific' => $entry->groupSpecificLocation
                    ] : [],
                    'refs'=> array_merge($categoryIds, $areaIds, $interestIds)
                ];
            }
        ],
        'api/groups/<filterids:\d+(?:[,]\d*)*>.json' => function ($filterids) {
            $relatedTo = ['and'];
            if (!empty($filterids)) {
                $ids = explode(',',$filterids);
                foreach ( $ids as $value) {
                    array_push($relatedTo,["targetElement" => $value]);
                }//$relatedTo = ['and',["targetElement" => explode(',',$filterids)]];
            }

            return [
                'elementType' => 'Entry',
                'paginate' => false,
                'criteria' => [
                    'section' => 'groups', 
                    'relatedTo' => $relatedTo,
                    'order' => 'postDate DESC',
                    'with' => ['groupCategory'],
                    'locale' => null
                ],
                'transformer' => function(EntryModel $entry) {

                    $category = [];
                    foreach ($entry->groupCategory as $cat)
                        $category[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'locale' => $entry->locale,
                        'groupBegins' => $entry->groupBegins ? $entry->groupBegins->format('M d, Y') : '',
                        'groupHideBegins' => $entry->groupHideBegins,
                        'groupMeetingSchedule' => $entry->groupMeetingSchedule,
                        'childcare' => $entry->kidsWelcome,
                        'groupCategory' => $category,
                        'groupArea' => isset($entry->groupArea[0]) ? $entry->groupArea[0]->title : '',
                        'location' => isset($entry->groupLocation[0]) ? [
                            'title' => $entry->groupLocation[0]->title,
                            'mapUrl' => "http://maps.google.com/?q=". $entry->groupLocation[0]->fullAddress(),
                            'specific' => $entry->groupSpecificLocation
                        ] : []
                    ];
                },
            ];
        },
        'api/groups/<section:(.*)\w>/<filterids:\d+(?:[,]\d*)*>.json' => function ($section, $filterids) {

            $relatedTo = ['and'];
            if (!empty($filterids)) {
                $targets = ['and'];
                foreach (explode(',',$filterids) as $value) {
                    array_push($targets,["targetElement" => $value]);
                }

                $criteria = craft()->elements->getCriteria(ElementType::Entry);
                $criteria->section = 'groups';
                $criteria->relatedTo = $targets;
                $criteria->limit = null;

                $entryIds = $criteria->ids();

                array_push($relatedTo,['sourceElement' => $entryIds]);
            } else {
                $criteria = craft()->elements->getCriteria(ElementType::Entry);
                $criteria->section = 'groups';
                $criteria->limit = null;

                $entryIds = $criteria->ids();

                array_push($relatedTo,['sourceElement' => $entryIds]);
            }
            
            return [
                'elementType' => 'Entry',
                'paginate' => false,
                'criteria' => [
                    'section' => $section, 
                    'limit' => null,
                    'order' => 'title ASC',
                    'relatedTo' => $relatedTo
                ],
                'transformer' => function(EntryModel $entry) {
                    return [
                        'label' => $entry->title,
                        'slug' => $entry->slug,
                        'id' => $entry->id,
                        'overview' => $entry->groupOverview ? $entry->groupOverview : ''
                    ];
                }
            ];
        },
        'api/groups/<section:(.*)\w>/all.json' => function ($section) {
            $relatedTo = ['and'];
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'groups';
            $criteria->limit = null;
            $entryIds = $criteria->ids();
            array_push($relatedTo,['sourceElement' => $entryIds]);
            
            return [
                'elementType' => 'Entry',
                'paginate' => false,
                'criteria' => [
                    'section' => $section, 
                    'limit' => null,
                    'order' => 'title ASC',
                    'relatedTo' => $relatedTo
                ],
                'transformer' => function(EntryModel $entry) {
                    return [
                        'label' => $entry->title,
                        'slug' => $entry->slug,
                        'id' => $entry->id,
                        'overview' => $entry->groupOverview ? $entry->groupOverview : ''
                    ];
                }
            ];
        },
        'api/groups/search/<query:(.*)>.json' => function ($query) {
            return [
                'elementType' => 'Entry',
                'paginate' => false,
                'criteria' => [
                    'section' => 'groups', 
                    'search' => '*' . $query . '*', 
                    'order' => 'postDate DESC',
                    'with' => ['groupCategory'], 
                    'limit' => 50
                ],
                'transformer' => function(EntryModel $entry) {

                    $category = [];
                    foreach ($entry->groupCategory as $cat)
                        $category[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'locale' => $entry->locale,
                        'groupBegins' => $entry->groupBegins ? $entry->groupBegins->format('M d, Y') : '',
                        'groupHideBegins' => $entry->groupHideBegins,
                        'groupMeetingSchedule' => $entry->groupMeetingSchedule,
                        'groupCategory' => $category,
                        'groupArea' => isset($entry->groupArea[0]) ? $entry->groupArea[0]->title : '',
                        'location' => isset($entry->groupLocation[0]) ? [
                            'title' => $entry->groupLocation[0]->title,
                            'mapUrl' => "http://maps.google.com/?q=". $entry->groupLocation[0]->fullAddress(),
                            'specific' => $entry->groupSpecificLocation
                        ] : []
                    ];
                }
            ];
        },
        'api/featuredgroups/<filterids:\d+(?:[,]\d*)*>.json' => function ($filterids) {
            
            if (!empty($filterids)) {
                $relatedTo = ["targetElement" => $filterids];
            }
            return [
                'elementType' => 'Entry',
                'paginate' => false,
                'criteria' => [
                    'section' => 'groups', 
                    'relatedTo' => $relatedTo, 
                    'order' => 'postDate DESC',
                    'groupFeatured' => 1,
                    'with' => ['groupCategory','groupArea'], 
                    'limit' => 6
                ],
                'transformer' => function(EntryModel $entry) {
                    $imageUrl = "";
                    if (isset($entry->groupImage[0])) {
                        $srcImage = $entry->groupImage[0];
                        if ($srcImage)
                        {
                            craft()->config->set('generateTransformsBeforePageLoad', true);
                            $imageUrl = $srcImage->getUrl([
                                'width' => 267, 
                                'height' => 267, 
                                'mode' => 'crop', 
                                'quality' => 75, 
                                'position' => 'center-center'
                            ]);
                        }
                    }

                    $category = [];
                    foreach ($entry->groupCategory as $cat)
                        $category[] = $cat->title;

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'imageUrl' => $imageUrl,
                        'locale' => $entry->locale,
                        //'groupOverview' => $entry->groupOverview,
                        'groupBegins' => $entry->groupBegins ? $entry->groupBegins->format('M d, Y') : '',
                        'groupHideBegins' => $entry->groupHideBegins,
                        'groupMeetingSchedule' => $entry->groupMeetingSchedule,
                        'groupCategory' => $category,
                        'groupArea' => isset($entry->groupArea[0]) ? $entry->groupArea[0]->title : '',
                        'location' => isset($entry->groupLocation[0]) ? [
                            'title' => $entry->groupLocation[0]->title,
                            'mapUrl' => "http://maps.google.com/?q=". $entry->groupLocation[0]->fullAddress(),
                            'specific' => $entry->groupSpecificLocation
                        ] : []
                    ];
                }
            ];
        },
    ]
];