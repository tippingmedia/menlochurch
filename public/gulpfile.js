// var autoprefixer = require('autoprefixer');
// var browserSync = require('browser-sync').create();
// var cheerio = require('gulp-cheerio');
// var cssnano = require('cssnano');
// var gulp = require('gulp');
// var imagemin = require('gulp-imagemin');
// var include = require('gulp-include');
// var newer = require('gulp-newer');
// var perfectionist = require('perfectionist');
// var plumber = require('gulp-plumber');
// var postcss = require('gulp-postcss');
// var rename = require('gulp-rename');
// var sass = require('gulp-sass');
// var svgstore = require('gulp-svgstore');
// var critical = require('critical');

const pkg = require('./package.json');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')({
    pattern: ['*'],
    scope: ['devDependencies']
});

var paths = {
    icons: {
        src: 'assets/icons/**/*',
        dest: 'assets/dist/icons',
        watch: 'assets/icons/**/*'
    },
    img: {
        src: ['assets/img/**/*', '!assets/img/logo-multi.svg', '!assets/img/logo-single.svg'],
        dest: 'assets/dist/img',
        watch: 'assets/img/**/*'
    },
    scss: {
        src: 'assets/scss/*.scss',
        dest: 'assets/dist/css',
        watch: 'assets/scss/**/*'
    },
    fonts: {
        src: 'assets/dist/fonts/*.scss',
        dest: 'assets/dist/fonts/dist'
    }
};

function icons() {
    return gulp.src(paths.icons.src)
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(rename(function(path) {
            var name = path.dirname.split(path.sep);
            name.push(path.basename);
            path.basename = name.join('-');
        }))
        .pipe(rename({ prefix: 'icon-' }))
        .pipe(cheerio({
            run: function($) {
                $('[fill]').removeAttr('fill');
                $('[fill-rule]').removeAttr('fill-rule');
                $('[clip-rule]').removeAttr('clip-rule');
                $('[data-name]').removeAttr('data-name');
                $('title').remove();
            },
            parserOptions: { xmlMode: true },
        }))
        .pipe($.svgstore({ inlineSvg: true }))
        .pipe($.cheerio(function($) {
            $('svg').attr('display', 'none');
        }))
        .pipe(gulp.dest(paths.icons.dest));
};

function img() {
    return gulp.src(paths.img.src)
        .pipe(plumber())
        .pipe(newer(paths.img.dest))
        .pipe(imagemin())
        .pipe(gulp.dest(paths.img.dest));
};


function scss() {
    return gulp.src(paths.scss.src)
        .pipe($.plumber())
        .pipe($.include())
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.postcss([$.autoprefixer()]))
        .pipe(gulp.dest(paths.scss.dest))
        .pipe($.postcss([$.cssnano(), $.perfectionist({ format: 'compact' })]))
        .pipe($.rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.scss.dest));
};


function criticalcss(callback) {
    doSynchronousLoop(pkg.globs.critical, processCriticalCSS, () => {
        // all done
        callback();
    });
};

// Process data in an array synchronously, moving onto the n+1 item only after the nth item callback
function doSynchronousLoop(data, processData, done) {
    if (data.length > 0) {
        const loop = (data, i, processData, done) => {
            processData(data[i], i, () => {
                if (++i < data.length) {
                    loop(data, i, processData, done);
                } else {
                    done();
                }
            });
        };
        loop(data, 0, processData, done);
    } else {
        done();
    }
}

// Process the critical path CSS one at a time
function processCriticalCSS(element, i, callback) {
    const criticalSrc = pkg.urls.critical + element.url;
    const criticalDest = pkg.paths.templates + element.template + '_critical.min.css';

    $.fancyLog("-> Generating critical CSS: " + $.chalk.cyan(criticalSrc) + " -> " + $.chalk.green(criticalDest));
    $.critical.generate({
        src: criticalSrc,
        dest: criticalDest,
        inline: false,
        ignore: [],
        base: pkg.paths.dist.base,
        css: [
            pkg.paths.dist.css + pkg.vars.siteCssName,
        ],
        minify: true,
        width: 1200,
        height: 1200
    }, (err, output) => {
        if (err) {
            $.fancyLog($.chalk.magenta(err));
        }
        callback();
    });
}


function fontcss() {
    return gulp.src(paths.fonts.src)
        .pipe($.plumber())
        .pipe($.include())
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.postcss([$.autoprefixer()]))
        .pipe(gulp.dest(paths.fonts.dest))
        .pipe($.browserSync.stream())
        .pipe($.postcss([$.cssnano(), $.perfectionist({ format: 'compact' })]))
        .pipe($.rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.fonts.dest));
};


function watch() {
	gulp.watch(['assets/scss/**/*'], gulp.series(scss));
}

exports.scss = scss;
exports.fonts = fontcss;
exports.icons = icons;
exports.img = img;
exports.critical = criticalcss;
exports.default = watch;