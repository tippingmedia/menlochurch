import Routes from './routes.js';
import axios from 'axios';
import modals from 'modals';
import mdls from './modals';

class LoadModal extends Routes {
    constructor() {
        super();
        const $this = this;

        this.routes = {
            "/modal/:type/:id/:locale": function(req, e) {
                const type = req.params.type;
                const id = req.params.id;
                const locale = req.params.locale;
                const baseUrl = window.campusBaseUrls[locale];
                const siteUrl = document.querySelector('body').dataset.baseUrl;
                const modalId = `#modal-${type}-${id}`;
                const url = locale !== 'mlo' ? `/${baseUrl}/api/modals/${type}/${id}?locale=${document.body.dataset.campus}` : `${siteUrl}api/modals/${type}/${id}?locale=${document.body.dataset.campus}`;

                if (document.querySelector(modalId) === null) {
                    axios(url).then((data) => {
                        let divElm = document.createElement('div');
                        divElm.innerHTML = data.data;
                        document.body.appendChild(divElm.children[0]);

                        new mdls();

                        document.querySelector(modalId).addEventListener('click', function(e) {
                            if (!e.target.closest('.modal_window')) {
                                modals.closeModal();
                            }
                        }, false);

                        modals.openModal(null, modalId);
                    });

                } else {
                    modals.openModal(null, modalId);
                }
            }
        };

        this.listen();
    }
}

export { LoadModal as default };