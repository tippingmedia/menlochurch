import Grapnel from "Grapnel";

class Routes {
    constructor() {
        this.router = new Grapnel();
        this.routes = {};

        this.listen();
    }

    listen() {
        Grapnel.listen(this.routes);
    }

    nav(url) {
        this.router.navigate(url);
    }
}

export { Routes as default };