require('./storage');

class Notify {
    constructor() {
        this.alerts = document.querySelector('.header_notificationsBox ul');
        this.viewedIds = localStorage.getObj('notification.viewed') ? localStorage.getObj('notification.viewed') : [];
        this.selector = document.querySelector('[data-notify-selector]');
        this.notifications = this.alerts.querySelectorAll('[data-notify-id]') !== null ? this.alerts.querySelectorAll('[data-notify-id]') : [];
        this.notifyCountElm = document.querySelector('.header_notificationsCount');
        this.count = 0;
        this.label = {
            "read": "-read",
            "unread": "-unread"
        }

        // If a notification has been viewed we need to show that.
        if (this.notifications.length) {
            for (const vid of this.viewedIds) {
                for (const elm of this.notifications) {
                    const id = elm.dataset.notifyId;
                    if (parseInt(vid) === parseInt(id)) {
                        this.disable(elm);
                    }
                }
            }
        } else {
            //Clear out old stored notifications if we don't have any in the drop-down
            if (this.viewedIds !== undefined && this.viewedIds.length) {
                localStorage.setObj('notification', { viewed: [] });
            }
        }


        this.hasUnread();
        this.setEvents();
        this.autoload();
    }

    setEvents() {
        const $this = this;

        document.addEventListener('click', function(event) {
            if (event.target.hasAttribute('data-notify-id') || event.target.closest('[data-notify-id]') !== null) {

                const btn = event.target.hasAttribute('data-notify-id') ? event.target : event.target.closest('[data-notify-id]');

                const id = btn.dataset.notifyId;
                if (!$this.viewedIds.includes(id)) {
                    $this.viewedIds.push(id);
                }
                localStorage.setObj('notification', { viewed: Array.from($this.viewedIds) });

                if ($this.notifications.length > 0) {
                    for (const elm of $this.notifications) {
                        if (parseInt(elm.dataset.notifyId) === parseInt(id)) {
                            $this.disable(elm);
                        }
                    }
                }
            }
        }, false);

        this.selector.addEventListener('click', function(evt) {
            let notifs = $this.selector.nextElementSibling;
            if (notifs.classList.contains('collapse')) {
                notifs.classList.remove('collapse');
            } else {
                notifs.classList.add('collapse');
            }
            evt.preventDefault();
        });

        // Close notifications on outside click
        document.addEventListener('click', function(evt) {
            if (evt.target.closest('.header_notifications') === null &&
                !document.querySelector('#notifications').classList.contains('collapse')) {

                document.querySelector('#notifications').classList.add('collapse');
            }
        }, false);
    }

    disable(elm) {
        elm.classList.remove(this.label.unread);
        elm.classList.add(this.label.read);
        this.hasUnread();
    }

    hasUnread() {
        const navAlert = document.querySelector('.header_notifications');

        const unread = this.alerts.querySelectorAll('.-unread');
        if (unread.length > 0) {
            navAlert.classList.add('-hasUnread');
        } else {
            if (navAlert.classList.contains('-hasUnread')) {
                navAlert.classList.remove('-hasUnread');
            }
        }
        this.count = unread.length;
        this.notifyCountElm.innerHTML = this.count;
        this.notifyCountElm.dataset.count = this.count;
    }

    autoload() {
        const alertId = document.querySelector('body').dataset.notification.trim();
        const autoLoadOn = document.querySelector('body').dataset.onautoload.trim();

        if (this.isNumeric(alertId)) {
            if (parseInt(autoLoadOn) === 1) {
                window.location.hash = `/modal/alert/${alertId}/${window.currentLocale}`;
            } else {
                if ($.inArray(alertId, this.viewedIds) === -1) {
                    window.location.hash = `/modal/alert/${alertId}/${window.currentLocale}`;
                }
            }
        }
    }

    isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
}

export { Notify as default }