import Flickity from 'flickity';
class TextSlider {
    constructor(elm) {
        const _this = this;
        this.elm = elm;
        this.nav = this.elm.querySelector('.textSlider_nav');
        this.navItems = this.nav.querySelectorAll('li');
        this.slides = this.elm.querySelectorAll('.textSlider_slides');

        this.flkty = new Flickity(this.slides[0], {
            cellAlign: 'left',
            wrapAround: false,
            pageDots: false,
            prevNextButtons: false,
            selectedAttraction: 0.2,
            friction: 0.8,
        });


        for (let [idx, navItem] of this.navItems) {
            navItem.addEventListener('click', function(e) {
                e.preventDefault();
                const index = idx;
                flkty.select(index);
            }, false);
        }

        this.flkty.addEventListener('select', function(e) {
            _this.navItems.filter(navitem => navitem.classList.contains('-selected')).classList.remove('-selected');
            _this.navItems[_this.flkty.selectedIndex].classList.add('-selected');
        }, false);
    }
}

export { TextSlider as default };

/*$('.textSlider').each(function() {
    var $this = $(this);
    var $nav = $this.find('.textSlider_nav');
    var $navItems = $nav.find('li');
    var $slides = $this.find('.textSlider_slides');

    var flkty = new Flickity($slides[0], {
        cellAlign: 'left',
        wrapAround: false,
        pageDots: false,
        prevNextButtons: false,
        selectedAttraction: 0.2,
        friction: 0.8,
    });

    $navItems.on('click', function(e) {
        e.preventDefault();
        var index = $(this).index();
        flkty.select(index);
    });

    flkty.on('select', function() {
        $navItems.filter(navitem => navitem.classList.contains('-selected')).classList.remove('-selected');
        $navItems.eq(flkty.selectedIndex).addClass('-selected');
    });
});*/