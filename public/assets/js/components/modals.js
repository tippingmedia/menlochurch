import ContactForm from './contactform';
import Flickity from 'flickity';
import AudioPlayer from './AudioPlayer';
import modals from 'modals';
import tabby from 'tabby';
import autosize from 'autosize';
import {
    fluidvids,
    modalsConfig,
    tabbyConfig
} from '../../blocs/js/blocs';

class Modals {
    constructor() {
        const $this = this;
        this.audioplayers = [];

        modalsConfig.callbackOpen = function(toggle, modal) {
            if (toggle !== null && toggle !== undefined) {
                window.locationRoute = toggle.hasAttribute('data-route') ? toggle.dataset.route : window.locationRoute;
            }

            //window.dispatchEvent(new Event('resize'));
            let modalevent = new CustomEvent(
                "modalopen", {
                    detail: {
                        id: modal.id,
                        time: new Date(),
                    },
                    bubbles: true,
                    cancelable: true
                }
            );

            document.dispatchEvent(modalevent);

            modal.addEventListener('click', function(e) {
                if (!e.target.closest('.modal_window')) {
                    modals.closeModal({
                        callbackClose: function() {
                            // Pause audio player 
                            $this.audioplayers.forEach(function(obj) {
                                obj.player.pause();
                            }, this);

                            var hash = window.location.hash;
                            if (hash == "#modal-contact" || hash == "#notifications" || hash.indexOf("#/modal/") == 0 || hash.indexOf("#tab-") == 0 || hash == "#modal-locations-dropdown") {
                                history.pushState("", document.title, window.location.pathname + window.location.search);
                            }

                        }
                    });
                }
            }, false);

            //Fix non-rendered map
            // if (modal.querySelector('#map-canvas') !== null) {
            //     google.maps.event.trigger(locations.gmap, 'resize');
            //     locations.gmap.fitBounds(locations.bounds);
            // }


            // initiate tabby plugin 
            tabbyConfig.callback = function() {
                autosize.update(document.querySelector('.modal.-active textarea'));

                let activeModal = document.querySelector('.modal.-active');
                if (activeModal !== null) {
                    activeModal.scrollTop = 0;
                }
            };

            tabby.init(tabbyConfig);


            // init contact forms
            if (modal.querySelectorAll('form.form').length > 0) {
                let forms = modal.querySelectorAll('form.form');
                for (let i = 0; i < forms.length; i++) {
                    // prevent modal-contact from getting initiated more than once.
                    if (forms[i].closest('#modal-contact') === null) {
                        let cform = new ContactForm(forms[i]);
                    }
                }
            }

            if (modal.querySelector('form.searchBox') !== null) {
                let search = modal.querySelector('form.searchBox input');
                search.addEventListener('focusin', function() {
                    document.body.scrollTop = 0;
                    let activeModal = document.querySelector('.modal.-active');
                    if (activeModal) {
                        activeModal.scrollTop = 0;
                    }
                });
            }


            const modalAudioPlayerId = `modal-${modal.id}`;
            if (modal.querySelector('.AudioPlayer') !== null && $this.audioplayers.find(d => d.id === modalAudioPlayerId) === undefined) {
                let audioElm = modal.querySelector('audio');
                let controls = modal.querySelector('.AudioPlayer');
                let player = new AudioPlayer(audioElm, controls);
                //$this.audioplayers[modalAudioPlayerId] = player;
                $this.audioplayers.push({ id: modalAudioPlayerId, player });
            }

            if (modal.querySelector('.modal_slider') !== null) {
                //let sliderElm = modal.querySelector('.modal_slider');
                let $prev = modal.querySelector('.modal_sliderArrow-prev');
                let $next = modal.querySelector('.modal_sliderArrow-next');
                let $slides = modal.querySelectorAll('.modal_sliderSlides');

                let slider = new Flickity($slides[0], {
                    initialIndex: 1,
                    wrapAround: false,
                    pageDots: false,
                    prevNextButtons: false,
                    setGallerySize: false,
                    draggable: false,
                });

                slider.on('select', function() {
                    if (slider.selectedIndex < 1) {
                        slider.select(1);
                    }
                });

                $prev.addEventListener('click', function(e) {
                    e.preventDefault();
                    slider.previous();
                }, false);

                $next.addEventListener('click', function(e) {
                    e.preventDefault();
                    slider.next();
                }, false);
            }

            // Open modals when an in-modal trigger is clicked
            let innermodals = modal.querySelectorAll('.modal [data-modal]');
            for (let mdl of innermodals) {
                mdl.addEventListener('click', function(evt) {
                    const _this = evt.target;
                    modals.closeModal();
                    modals.openModal(_this[0], _this.dataset.modal);
                }, false);
            }

            fluidvids.init();
        };

        modalsConfig.callbackClose = function(toggle, modal) {
            $this.audioplayers.forEach(function(obj) {
                obj.player.pause();
            }, this);

            var hash = window.location.hash;
            if (hash == "#modal-contact" || hash == "#notifications" || hash.indexOf("#/modal/") == 0 || hash.indexOf("#tab-") == 0 || hash == "#modal-locations-dropdown") {
                history.pushState("", document.title, window.location.pathname + window.location.search);
            }
        };

        modals.init(modalsConfig);


        // Close modal when .modal (but not .modal_window) is clicked


        window.addEventListener("hashchange", function(evt) {
            let hash = window.location.hash;
            const ids = ['#modal-contact'];
            if (ids.includes(hash)) {
                modals.openModal(null, hash);
            }
        }, false);




        /*$('.modal [data-modal]').on('click', function() {
            var $this = $(this);
            modals.closeModal();
            modals.openModal($this[0], $this.data('modal'));
        });*/
    }
}
export { Modals as default };