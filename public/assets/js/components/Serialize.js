class Serialize {
    constructor() {

    }

    values(formElem) {
        let obj = {};
        let inputElems = formElem.querySelectorAll("input, select, textarea");

        for (let i = 0; i < inputElems.length; i++) {
            this.serializeInputElement(inputElems.item(i), obj);
        }

        return obj;
    }

    getInputElementValue(inputElem, uncheckedAsFalse) {
        var type = inputElem.getAttribute("type");

        if ((type === "button") || (type === "submit")) {
            return null;
        }

        if (inputElem instanceof HTMLInputElement) {
            if (type === "checkbox") {
                uncheckedAsFalse = uncheckedAsFalse;

                if (!(uncheckedAsFalse === undefined ? defaultConfigs.uncheckedAsFalse : uncheckedAsFalse)) {
                    if (!inputElem.checked) {
                        return null;
                    }
                }

                return inputElem.getAttribute("value") == null ? inputElem.checked : (inputElem.checked ? inputElem.value : null);
            } else if (type === "radio") {
                if (!inputElem.checked) {
                    return null;
                }

                return inputElem.value;
            } else if (type === "number") {
                return Number(inputElem.value);
            } else {
                return inputElem.value;
            }
        } else if (inputElem instanceof HTMLSelectElement) {
            if ((inputElem.options.length > 0) && (inputElem.selectedIndex >= 0)) {
                return inputElem.options[inputElem.selectedIndex].value;
            } else {
                return null;
            }
        } else if (inputElem instanceof HTMLTextAreaElement) {
            return inputElem.value;
        }

        return null;
    }

    serializeInputElement(inputElem, obj) {
        var name = (inputElem instanceof Element) ? inputElem.getAttribute("name") : inputElem.name;

        if ((!name) || (name.length <= 0)) {
            return;
        }

        var value = (inputElem instanceof Element) ? this.getInputElementValue(inputElem) : inputElem.value;

        if ((value === null) || (value === undefined)) {
            return;
        }

        if (!obj) {
            obj = {};
        }

        var objRef = obj;
        var objRefParentArray = null;
        var objRefParentArrayIndex = 0;
        var lastNameStart = 0;
        var finalFieldName;
        var partName;
        var len;

        for (var i = 0; i < name.length; i++) {
            if (name[i] === ".") {
                partName = name.substring(lastNameStart, i).replace(/\[(.*?)\]/g, "");

                if ((!partName) || (partName.length <= 0)) {
                    continue;
                }

                if (!objRef[partName]) {
                    objRef[partName] = {};
                    objRef = objRef[partName];
                } else if (objRef[partName] instanceof Array) {
                    len = objRef[partName].push({});
                    objRef = objRef[partName][len - 1];
                } else {
                    objRef = objRef[partName];
                }

                objRefParentArray = null;
                lastNameStart = i + 1;
            } else if (name[i] === "]") {
                var indexBegin = i - 1;

                while (name[indexBegin] !== "[") {
                    indexBegin--;
                }

                var indexStr = name.substring(indexBegin + 1, i);
                partName = name.substring(lastNameStart, indexBegin);

                if (partName[0] === ".") {
                    partName = partName.substring(1);
                }

                if (!objRef[partName]) {
                    objRef[partName] = [];
                }

                if (indexStr === "") {
                    len = objRef[partName].push({});
                    objRefParentArray = objRef[partName];
                    objRefParentArrayIndex = len - 1;
                    objRef = objRef[partName][len - 1];
                } else {
                    var arrIndex = Number(indexStr);
                    objRefParentArray = objRef[partName];
                    objRefParentArrayIndex = arrIndex;

                    if (!objRef[partName][arrIndex]) {
                        objRef[partName][arrIndex] = {};
                    }

                    objRef = objRef[partName][arrIndex];
                }

                lastNameStart = i + 1;
            }
        }

        if (lastNameStart === 0) {
            finalFieldName = name;
        } else {
            finalFieldName = name.substring(lastNameStart);
        }

        while (finalFieldName[0] === ".") {
            finalFieldName = finalFieldName.substring(1);
        }

        if (objRefParentArray !== null) {
            if (name[name.length - 1] !== "]") {
                var lastArrayObjIndex = objRefParentArrayIndex - 1;
                var lastArrayObj = objRefParentArray[lastArrayObjIndex];

                while ((lastArrayObj === undefined) && (lastArrayObjIndex >= 0)) {
                    lastArrayObjIndex--;
                    lastArrayObj = objRefParentArray[lastArrayObjIndex];
                }

                if ((lastArrayObj !== undefined) && (lastArrayObj[finalFieldName] === undefined)) {
                    lastArrayObj[finalFieldName] = value;
                    objRefParentArray.splice(objRefParentArrayIndex, 1);
                } else {
                    objRef[finalFieldName] = value;
                }
            } else {
                objRefParentArray[objRefParentArrayIndex] = value;
            }
        } else {
            objRef[finalFieldName] = value;
        }

        return obj;
    }
}

export { Serialize as default };