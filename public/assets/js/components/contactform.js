import axios from 'axios';
//import Serialize from './Serialize';
import isEmail from 'validator/lib/isEmail';
import isEmpty from 'validator/lib/isEmpty';
import isMobilePhone from 'validator/lib/isMobilePhone';
import $ from 'jquery';

class ContactForm {
    constructor(formElm) {

        this.form = formElm;
        this.action = this.form.querySelector('input[name=action]').value;
        this.requiredInputs = this.form.querySelectorAll('[required], [data-check]');

        this.setup();
    }

    setup() {
        let _this = this;

        this.form.addEventListener('submit', function(evt) {
            evt.preventDefault();
            _this.formSending();
            //let params = new Serialize().values(_this.form);
            let params = $(_this.form).serializeArray();
            if (_this.validate()) {
                // $.post('/', params, function(response) {
                //     if (response.success) {
                //         _this.formSent();
                //         //_this.cleanup();
                //     } else {
                //         _this.formSendError();
                //     }
                // });

                $.ajax({
                    url: _this.action,
                    method: _this.form.getAttribute('method'),
                    data: params,
                    cache: false,
                    success: function(response) {
                        if (response.success) {
                            _this.formSent();
                        }
                    },
                    error: function(response) {
                        _this.formSendError();
                    }
                });
                /*axios({
                    method: 'post',
                    url: '/',
                    data: params
                }).then(response => {
                    console.log(response);
                    if (response.status === 200) {
                        _this.formSent();
                        _this.cleanup();
                    }
                }).catch(response => {
                    _this.formSendError();
                    console.log(response);
                });*/
            }
        }, false);
        for (var i = 0; i < this.requiredInputs.length; i++) {
            var input = this.requiredInputs[i];
            input.addEventListener('blur', function(evt) {
                if (evt.target.required) {
                    _this.validate(evt.target);
                }
            });
        }

    }

    validate(inputElm = null) {
        let _this = this;
        let status = true;
        let inputs = inputElm !== null ? [inputElm] : this.requiredInputs;

        for (let i = 0; i < inputs.length; i++) {
            let input = inputs[i];

            const type = input.getAttribute('type');
            const value = (input instanceof HTMLSelectElement) ? input.options[input.selectedIndex].value : input.value;
            let inputStatus = true;

            if (input.required) {
                if (isEmpty(value)) {
                    inputStatus = false;
                }
            }

            if (type === 'email') {
                if (!isEmail(value)) {
                    inputStatus = false;
                }
            }

            if (type === 'tel') {
                if (!isMobilePhone(value, 'any')) {
                    inputStatus = false;
                }
            }

            // if input is not valid then validate status is false
            if (!inputStatus) {
                _this.status = false;
            }

            // if input is valid clear errors else show errors
            const action = inputStatus ? 'valid' : 'error';

            this.validateMessage(input, action);
        }

        if (!status) {
            _this.formError();
        }

        return status;
    }

    validateMessage(input, action) {
        //let parent = input.parentNode();
        //let messageWrap = (input instanceof HTMLTextAreaElement) ? input.nextNode() : parent.nextNode();

        if (action === 'valid') {
            if (input instanceof HTMLInputElement || input instanceof HTMLSelectElement) {
                input.parentNode.classList.remove('-error');
            }
            input.classList.remove('-error');
        }

        if (action === 'error') {
            if (input instanceof HTMLInputElement || input instanceof HTMLSelectElement) {
                input.parentNode.classList.add('-error');
            }
            input.classList.add('-error');
        }
    }

    formSending() {
        this.form.classList.remove('form-error', 'form-sent', 'form-send', 'form-senderror');
        this.form.classList.add('form-sending');
    }

    formSend() {
        this.form.classList.remove('form-error', 'form-sent', 'form-sending', 'form-senderror');
        this.form.classList.add('form-send');
    }

    formSent() {
        this.form.classList.remove('form-error', 'form-sending', 'form-send', 'form-senderror');
        this.form.classList.add('form-sent');
    }

    formError() {
        this.form.classList.remove('form-sending', 'form-sent', 'form-send', 'form-senderror');
        this.form.classList.add('form-error');
    }

    formSendError() {
        this.form.classList.remove('form-sending', 'form-sent', 'form-send', 'form-error');
        this.form.classList.add('form-senderror');
    }


    cleanup() {
        const _this = this;
        let dirtyInputs = this.requiredInputs;
        let timeout = setTimeout(() => {
            for (var i = 0; i < dirtyInputs.length; i++) {
                var input = dirtyInputs[i];
                if (input instanceof HTMLSelectElement) {
                    input.selectedIndex = 0;
                } else {
                    input.value = "";
                }
            }
            _this.formSend();
            window.clearTimeout(timeout);

        }, 3000);
    }

}

export { ContactForm as default };