import './components/storage.js';
import {
    fluidvids,
    houdini,
    houdiniConfig,
    tabbyConfig,
} from '../blocs/js/blocs';

import $ from 'jquery';
import asNavFor from 'flickity-as-nav-for';
import Flickity from 'flickity';
import Headroom from 'headroom.js';
import objectFitPolyfill from 'objectFitPolyfill';
import Rellax from 'rellax';
import isMobile from 'ismobilejs';
import autosize from 'autosize';
import Chartist from 'chartist';
import CountUp from 'countup.js';
import tabby from 'tabby';
import _ from 'lodash';
import SmoothScroll from "smooth-scroll";

import axios from 'axios';
import loadmodal from './components/loadmodal';
import Notify from './components/notifications';
import ContactForm from './components/contactform';
import TextSliders from './components/textsliders';
import modals from './components/modals';


document.addEventListener('DOMContentLoaded', function() {

    // TOUCH DETECTION
    function is_touch_device() {
        var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
        var mq = function(query) {
            return window.matchMedia(query).matches;
        }
        if (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch) &&
            (deviceAgent.match(/(iphone|ipod|ipad)/) ||
                deviceAgent.match(/(android)/) ||
                deviceAgent.match(/(iemobile)/) ||
                deviceAgent.match(/iphone/i) ||
                deviceAgent.match(/ipad/i) ||
                deviceAgent.match(/ipod/i) ||
                deviceAgent.match(/blackberry/i) ||
                deviceAgent.match(/bada/i))
        ) {
            return true;
        }
        // include the 'heartz' as a way to have a non matching MQ to help terminate the join
        // https://git.io/vznFH
        var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
        return mq(query);
    }

    if (is_touch_device()) {
        document.querySelector('body').classList.add('istouch');
    } else {
        document.querySelector('body').classList.add('nontouch');
    }

    // --------------------------------------------------------------------------

    houdiniConfig.callbackOpen = function(content) {
        if (content.classList.contains('-active')) {
            content.parentElement.classList.add('-active');
        } else {
            content.parentElement.classList.remove('-active');
        }
    };

    houdiniConfig.callbackClose = function(content) {
        if (content.classList.contains('-active')) {
            content.parentElement.classList.add('-active');
        } else {
            content.parentElement.classList.remove('-active');
        }
    };

    houdini.init(houdiniConfig);

    // --------------------------------------------------------------------------

    var $header = document.querySelector('.header');
    var stickyHeader = new Headroom($header, {
        offset: 40
    });

    stickyHeader.init();

    // --------------------------------------------------------------------------

    if ($('.rellax').length && !isMobile.any) {
        var rellax = new Rellax('.rellax', {
            center: true,
        });
    }


    // --------------------------------------------------------------------------

    new Notify();

    // --------------------------------------------------------------------------

    if (document.querySelectorAll('.form') !== null) {
        let forms = document.querySelectorAll('.form');
        for (let i = 0; i < forms.length; i++) {
            new ContactForm(forms[i]);
        }
    }

    // --------------------------------------------------------------------------

    fluidvids.init();

    // --------------------------------------------------------------------------
    tabbyConfig.callback = function(tabs, toggle) {
        //console.log(tabs);
        //console.log(toggle);
        autosize.update(document.querySelector('.modal.-active textarea'));

        let activeModal = document.querySelector('.modal.-active');
        if (activeModal !== null) {
            activeModal.scrollTop = 0;
        }

        // Search Tabs
        if (toggle !== null) {
            if (toggle.dataset.tabSearch !== null || toggle.dataset.tabSearch !== undefined) {
                let section = toggle.dataset.tabSearch;

                let pane = document.querySelector(`${toggle.getAttribute('href')}`);
                let query = toggle.parentNode.dataset.tabSearchQuery;
                if(pane !== null) {                
                    let output = pane.querySelector('ul');
                    const locale = document.querySelector('body').dataset.campus;
                    const baseUrl = window.campusBaseUrls[locale];
                    const loader = pane.querySelector('svg');
                    if (output !== null && output.childNodes.length < 1) {
                        loader.style.display = "block";
                        axios.get(`${baseUrl == "/" ? "" : "/" + baseUrl}/api/search/${section}/${query}`).then(response => {
                            if (response.data.trim() === "") {
                                toggle.style.textDecoration = "line-through";
                                output.innerHTML = `<li><p> Sorry, we didn't find any results for <strong>${query}</strong>.</p></li>`;
                            } else {
                                output.innerHTML = response.data;
                            }
                            loader.style.display = "none";

                        }).catch(response => { console.log(response); });
                    }
                }
            }
        }
    };

    tabby.init(tabbyConfig);

    // --------------------------------------------------------------------------


    // modals loaded via ajax
    new modals();
    new loadmodal();




    // Select File Download
    let downloadSelector = document.querySelector('[data-download]');
    if (downloadSelector !== null) {
        downloadSelector.addEventListener('change', function(evt) {
            let value = evt.target.options[evt.target.selectedIndex].value;
            let link = document.querySelector(`a[data-did="${value}"]`);
            link.click();
        });
    }

    // --------------------------------------------------------------------------

    let scrolls = document.querySelectorAll('[data-scrollto]');
    if (scrolls.length > 0) {
        window.addEventListener('click', function(evt) {
            if (event.target.hasAttribute('data-scrollto') || evt.target.closest('[data-scrollto]') !== null) {
                evt.preventDefault();
                let target = evt.target.hasAttribute('data-scrollto') ? evt.target.href.split("#") : evt.target.closest('[data-scrollto]').href.split("#");
                let scroll = new SmoothScroll();
                let options = { speed: 1000, easing: 'easeOutCubic' };
                scroll.animateScroll(target, options);
            }
        }, false);
    }

    // --------------------------------------------------------------------------

    $(document).on('focus blur', '.floatLabel input', function() {
        var $this = $(this);

        // Trim the value so spaces don't get counted as filled
        $this.val($this.val().trim());

        if ($this.val() !== '') {
            $this.addClass('-filled');
        } else {
            $this.removeClass('-filled');
        }
    });

    // --------------------------------------------------------------------------

    $('.locationsDropdown_close').on('click', function() {
        var id = '#' + $(this).closest('.locationsDropdown').attr('id');
        var toggle = $('[data-collapse][href="' + id + '"]')[0];
        houdini.closeContent(id, toggle);
    });

    // --------------------------------------------------------------------------

    var setCookie = function(name, value, days) {
        var seconds = days * 86400;
        if (typeof(seconds) != 'undefined') {
            var date = new Date();
            date.setTime(date.getTime() + (seconds * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else {
            var expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    };

    let parseURL = function(url) {
        var a = document.createElement('a');
        a.href = url;
        return {
            source: url,
            protocol: a.protocol.replace(':', ''),
            host: a.hostname,
            port: a.port,
            query: a.search,
            params: (function() {
                var ret = {},
                    seg = a.search.replace(/^\?/, '').split('&'),
                    len = seg.length,
                    i = 0,
                    s;
                for (; i < len; i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),
            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [, ''])[1],
            hash: a.hash.replace('#', ''),
            path: a.pathname.replace(/^([^\/])/, '/$1'),
            relative: (a.href.match(/tp:\/\/[^\/]+(.+)/) || [, ''])[1],
            segments: a.pathname.replace(/^\//, '').split('/')
        };
    };

    var locations = {

        $el: $('.locations'),
        $elList: $('.locations_list'),
        $elMap: $('#map-canvas'),

        data: [],
        bounds: null,
        gmap: null,
        popups: {},
        markers: {},
        icon: '/assets/dist/img/map-pin.svg',
        styles: [{ "featureType": "administrative", "elementType": "all", "stylers": [{ "saturation": "-100" }, { "gamma": "0.31" }, { "visibility": "on" }, { "color": "#ffffff" }] }, { "featureType": "administrative", "elementType": "labels", "stylers": [{ "visibility": "simplified" }, { "weight": "1.82" }] }, { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "weight": "1.34" }] }, { "featureType": "administrative", "elementType": "labels.text.stroke", "stylers": [{ "color": "#5f5f5f" }] }, { "featureType": "administrative.province", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": "48" }, { "visibility": "on" }, { "color": "#c0bfbf" }] }, { "featureType": "landscape.natural.landcover", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.natural.terrain", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": "50" }, { "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "color": "#757575" }, { "weight": "2.29" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "on" }, { "weight": "0.70" }] }, { "featureType": "road.highway", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.arterial", "elementType": "all", "stylers": [{ "lightness": "30" }, { "color": "#757575" }, { "visibility": "off" }] }, { "featureType": "road.arterial", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.local", "elementType": "all", "stylers": [{ "lightness": "40" }, { "visibility": "off" }] }, { "featureType": "road.local", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "saturation": -100 }, { "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#8d8c8c" }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "lightness": -25 }, { "saturation": -100 }] }],

        templateItem: _.template(document.getElementById('template-locations-item').innerHTML),
        templatePopup: _.template(document.getElementById('template-locations-popup').innerHTML),

        init: function() {
            // If location data is in storage use that instead of ajax hit
            let _camps = localStorage.getObj('campuses.data');

            axios.get('/api/campuses.json').then(response => {
                locations.data = response.data.data;
                localStorage.setObj('campuses', { data: locations.data, posted: new Date().getTime() });
                locations.buildMap();
            }).catch(response => { console.log(response); });

            /*if (typeof _camps === 'undefined') {
                axios.get('/api/campuses.json').then(response => {
                    locations.data = response.data.data;
                    localStorage.setObj('campuses', { data: Array.from(locations.data) });
                    locations.buildMap();
                }).catch(response => { console.log(response); });
            } else {
                locations.data = _camps;
                locations.buildMap();
            }*/

            // if window less than 641 location item lick call url not map tool tip
            // -- 04/25/2018 temporary fix because google maps not working changing window size  to 2000.
            $(document).on('click', '.locations_item', function() {
                if (window.innerWidth < 641) return;
                locations.toggle($(this).data('location-id'));
            });


            document.getElementById("modal-locations").addEventListener("click", function(e) {
                // e.target is our targetted element.
                let c_campus = document.querySelector('body').dataset.campus;
                let campuses = window.campusBaseUrls;

                if (
                    (e.target && e.target.nodeName == "BUTTON" && e.target.hasAttribute('data-location-id')) ||
                    (window.innerWidth < 641 && e.target && (e.target.hasAttribute('data-location-id') || e.target.closest('[data-location-id]') !== null))
                ) {

                    let id = e.target.dataset.locationId !== undefined ? e.target.dataset.locationId : e.target.closest('[data-location-id]').dataset.locationId;
                    setCookie("campus", id, 90);
                    //if (id !== c_campus) {
                    let regex = /\/(mlo-park\/|mlo-sm\/|mlo-mv\/|mlo-sj\/|mlo-st\/|mlo-sc\/|mlo-on\/|menlopark\/|mountainview\/|saratoga\/|sanmateo\/|sanjose\/|southcity\/|online\/)/ig;
                    let parsedURL = parseURL(window.location.href);
                    let curl = parseURL(window.location.href).path;
                    let matches = curl.match(regex);
                    let url = '';
                    //console.log(matches);
                    if (window.locationRoute === 'inline') {
                        if (matches !== null) {
                            url = `${parsedURL.source.replace(regex, "/"+campuses[id]+"/")}`;
                            //console.log(`No campus selected: ${url}`);
                        } else {
                            url = `${parsedURL.protocol}://${parsedURL.host}/${campuses[id]}${parsedURL.path}`;
                            //console.log(`Already has campus selected: ${url}`);
                        }
                    } else if (window.locationRoute === 'landing') {
                        url = `${parsedURL.protocol}://${parsedURL.host}/${campuses[id]}/campus/`;
                    }

                    window.location.assign(url);
                    //}
                }
            });
        },

        buildMap: function() {

            locations.gmap = new google.maps.Map(locations.$elMap[0], {
                mapTypeControl: false,
                scrollwheel: false,
                streetViewControl: false,
                styles: locations.styles,
            });

            locations.bounds = new google.maps.LatLngBounds();

            //_.forOwn(locations.data, function(location, id) {
            //});
            for (let location of locations.data) {
                if (location.slug !== "mlo") {
                    locations.$elList.append(locations.templateItem(location));
                    let locId = location.slug;
                    var popup = new InfoBox({
                        content: locations.templatePopup(location),
                        closeBoxURL: '',
                        boxStyle: {
                            width: '320px',
                        },
                        pixelOffset: new google.maps.Size(-160, -40),
                        alignBottom: true,
                        enableEventPropagation: true
                    });

                    var marker = new google.maps.Marker({
                        map: locations.gmap,
                        position: new google.maps.LatLng(parseFloat(location.lat), parseFloat(location.lng)),
                        title: location.name,
                        icon: locations.icon,
                    });

                    locations.popups[locId] = popup;
                    locations.markers[locId] = marker;

                    google.maps.event.addListener(marker, 'click', function(evt) {
                        locations.toggle(locId);
                    });

                    locations.bounds.extend(marker.position);
                }
            };


            google.maps.event.addDomListener(window, 'resize', function() {
                google.maps.event.trigger(locations.gmap, 'resize');
                locations.gmap.fitBounds(locations.bounds);
            });

            locations.gmap.fitBounds(locations.bounds);

            document.addEventListener('modalopen', function(evt) {
                if (evt.detail.id === "modal-locations") {
                    google.maps.event.trigger(locations.gmap, 'resize');
                    locations.gmap.fitBounds(locations.bounds);
                }
            }, false);
        },

        toggle: function(id) {
            var $item = $('.locations_item[data-location-id="' + id + '"]');
            var popup = locations.popups[id];
            var selected = $item.hasClass('-selected');

            locations.deselectAll();

            if (selected) {
                locations.gmap.fitBounds(locations.bounds);
            } else {
                $item.addClass('-selected');
                popup.open(locations.gmap, locations.markers[id]);
                let loc = locations.data.filter(loc => loc.slug === id);
                locations.gmap.setCenter(new google.maps.LatLng(loc[0].lat, loc[0].lng));
                //locations.gmap.setCenter(new google.maps.LatLng(locations.data[id].lat, locations.data[id].lng));
            }
        },

        deselectAll: function() {
            $('.locations_item').removeClass('-selected');

            _.forOwn(locations.popups, function(popup, id) {
                locations.popups[id].close();
            });
        },

    }


    locations.init();



    // --------------------------------------------------------------------------

    $('.textSlider').each(function() {

        let $this = $(this);
        let $imgs = $this.find('.textSlider_img');
        let $nav = $this.find('.textSlider_nav');
        let $navItems = $nav.find('li');
        let $slides = $this.find('.textSlider_slides');

        let flkty = new Flickity($slides[0], {
            cellAlign: 'left',
            wrapAround: false,
            pageDots: false,
            prevNextButtons: false,
            selectedAttraction: 0.2,
            friction: 0.8
        });

        $navItems.on('click', function(e) {
            e.preventDefault();
            var index = $(this).index();
            flkty.select(index);
        });

        flkty.on('select', function() {
            $imgs.filter('.-selected').removeClass('-selected');
            $imgs.eq(flkty.selectedIndex).addClass('-selected');
            $navItems.filter('.-selected').removeClass('-selected');
            $navItems.eq(flkty.selectedIndex).addClass('-selected');
        });

        $(window).on('load', function() {
            flkty.resize();
        });
    });

    // --------------------------------------------------------------------------

    $('.timelineSlider').each(function() {
        var $this = $(this);
        var $year = $this.find('.timelineSlider_year');
        var $nav = $this.find('.textSlider_nav');
        var $navItems = $nav.find('li');
        var $slides = $this.find('.textSlider_slides');
        var flkty = Flickity.data($slides[0]);

        flkty.on('select', function() {
            var year = $navItems.eq(flkty.selectedIndex).text();
            $year.text(year);
        });
    });

    // --------------------------------------------------------------------------

    $('.modal_slider').each(function() {
        var $this = $(this);
        var $prev = $this.find('.modal_sliderArrow-prev');
        var $next = $this.find('.modal_sliderArrow-next');
        var $slides = $this.find('.modal_sliderSlides');

        var flkty = new Flickity($slides[0], {
            initialIndex: 1,
            wrapAround: false,
            pageDots: false,
            prevNextButtons: false,
            setGallerySize: false,
            draggable: false,
        });

        flkty.on('select', function() {
            if (flkty.selectedIndex < 1) {
                flkty.select(1);
            }
        });

        $prev.on('click', function(e) {
            e.preventDefault();
            flkty.previous();
        });

        $next.on('click', function(e) {
            e.preventDefault();
            flkty.next();
        });
    });

    // --------------------------------------------------------------------------

    $('.timelineAlt').each(function() {
        var $this = $(this);
        var $dates = $this.find('.timelineAlt_dates');
        var $slides = $this.find('.timelineAlt_slides');
        var $prev = $this.find('.timelineAlt_arrow-prev');
        var $next = $this.find('.timelineAlt_arrow-next');
        var $progress = $this.find('.timelineAlt_progress');

        var flktyDates = new Flickity($dates[0], {
            asNavFor: $slides[0],
            pageDots: false,
            prevNextButtons: false,
            draggable: false,
        });

        var flktySlides = new Flickity($slides[0], {
            pageDots: false,
            prevNextButtons: false,
            selectedAttraction: 0.2,
            friction: 0.8,
        });

        $prev.on('click', function() {
            flktySlides.previous();
        });

        $next.on('click', function() {
            flktySlides.next();
        });

        function setProgress(important) {
            $dates.find('li:lt(' + flktySlides.selectedIndex + ')').addClass('-past');
            $dates.find('li:gt(' + (flktySlides.selectedIndex - 1) + ')').removeClass('-past');
        }

        setProgress();

        flktySlides.on('select', function() {
            setProgress();
        });
    });

    // --------------------------------------------------------------------------

    $('[data-countup]').each(function() {
        var $this = $(this);
        var data = $this.data('countup');

        var counter = new CountUp($this[0], 0, parseInt(data));
        counter.start();
    });

    // --------------------------------------------------------------------------

    $('[data-progress-pie]').each(function() {
        var $this = $(this);
        var data = $this.data('progress-pie');

        var filled = parseInt(data);
        var blank = parseInt(100 - filled);

        var chart = new Chartist.Pie($this[0], {
            series: [filled, blank],
        }, {
            donut: true,
            donutWidth: 5,
            chartPadding: 0,
            showLabel: false,
        });
    });

    // --------------------------------------------------------------------------

    $('[data-sliced-pie]').each(function() {
        var $this = $(this);
        var data = $this.data('sliced-pie');

        var labels = [];
        var series = [];

        _.forEach(data, function(item, i) {
            labels.push(item.label);
            series.push(parseInt(item.value));
        });

        var chart = new Chartist.Pie($this[0], {
            labels: labels,
            series: series,
        }, {
            donut: true,
            donutWidth: 60,
            chartPadding: 0,
            showLabel: false,
        });
    });

    // --------------------------------------------------------------------------

    $('[data-bar-graph]').each(function() {
        var $this = $(this);
        var data = $this.data('bar-graph');

        var labels = [];
        var series = [];

        _.forEach(data, function(set, i) {
            var setSeries = [];

            _.forEach(set.values, function(item, i2) {
                labels.push(item.label);
                setSeries.push(parseInt(item.value));
            });

            series.push(setSeries);
        });

        labels = _.uniq(labels);

        var chart = new Chartist.Bar($this[0], {
            labels: labels,
            series: series,
        }, {
            horizontalBars: true,
            seriesBarDistance: 30,
            reverseData: true,
            chartPadding: [0, 20, 0, 0],
            axisX: {
                scaleMinSpace: 40,
            },
            axisY: {
                offset: 120,
            },
        }, [
            ['screen and (min-width: 640px)', {
                axisX: {
                    scaleMinSpace: 80,
                },
                axisY: {
                    offset: 150,
                },
            }],
            ['screen and (min-width: 920px)', {
                axisX: {
                    scaleMinSpace: 160,
                },
                axisY: {
                    offset: 180,
                },
            }],
        ]);
    });

    const selectAnchors = document.querySelector('[data-select-anchor]');

    if (selectAnchors !== null) {
        selectAnchors.addEventListener('change', function(evt) {
            let value = evt.target.options[evt.target.selectedIndex].value;
            if (value !== undefined || value !== '') {
                let selector = `a[data-did='${value}']`;
                document.querySelector(selector).click();
            }
        }, false);
        //$("a[data-did="+value+"]")[0].click();

    }

});