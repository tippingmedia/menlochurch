import Vue from 'vue';
import moment from 'moment-timezone';
moment.tz.setDefault('UTC');
Object.defineProperty(Vue.prototype, '$moment', {
    get: function() {
        return this.$root.moment;
    }
});

import CalendarApp from './src/components/calendar/CalendarApp.vue';
import store from './src/store/calendar';

export default new Vue({
    el: '#calendarApp',
    store,
    data: {
        moment
    },
    components: {
        CalendarApp
    }
});