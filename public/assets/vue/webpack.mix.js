let mix = require('laravel-mix');

// use .extract(['vue'])  if using Vuejs
mix.js('./web.entry.js', 'web.bundle.js').setPublicPath('../dist/js/components');
    