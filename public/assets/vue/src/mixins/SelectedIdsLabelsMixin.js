export default {
    methods: {
        updateSelects(evt) {
            const form = document.querySelector('form.groupFinder');

            let selects = form.querySelectorAll('select');
            let ids = [];
            let labels = [];
            selects.forEach(element => {
                if (element.selectedIndex !== -1) {
                    let id = element.options[element.selectedIndex].value;
                    let label = element.options[element.selectedIndex].text;
                    let labelId = element.dataset.category;
                    if (id !== "#") {
                        ids.push(id);
                        labels[labelId] = label;
                    }
                }
            });

            this.$store.commit('setSelectedIds', ids);
            this.$store.commit('setSelectedLabels', labels);

            Event.$emit('UpdateGroupFiltersOptions', this.$store.state.step + 1);
            this.$store.dispatch('loadFilteredGroups', { ids: ids, viewChange: false });
        }
    }
}