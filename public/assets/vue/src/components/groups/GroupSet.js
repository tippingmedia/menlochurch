import Group from "./Group.vue";
export default {
    template: "#group-set-template",
    delimiters: ['${', '}'],
    props: ['groupset'],
    components: {
        Group
    }
}