import axios from 'axios';
export default {
    template: "#group-step-template",
    delimiters: ['${', '}'],
    props: ['stepid', 'group', 'heading'],
    data() {
        return {
            options: [],
            selectedLabel: `${this.heading}`,
            selectedId: "#",
            prevSelectedId: [Number, String],
            initialLoad: 1,
            nextDisabled: true,
            groupsLoading: false
        }
    },
    computed: {
        currentStep() {
            return this.$store.state.step;
        },
        totalGroups() {
            //console.log(this.$store.state.filteredGroups);
            return this.$store.state.filteredGroups.length;
        },
        classObject() {
            let stepclass = "groupFinder_step";
            return this.currentStep === this.stepid ? stepclass : stepclass + " display-none";
        },
        selectedLabels() {
            return this.$store.state.selectedLabels;
        },
        category() {
            return this.group;
        }
    },
    methods: {
        loadOptions() {
            let ids = this.$store.state.selectedIds.filter(n => n);
            const url = `/api/groups/${this.group}/${ids}.json`;
            axios.get(url).then(response => {
                this.options = response.data.data;
                Event.$emit('GroupLoading', false);
            });
        },
        loadAllOptions() {
            const _this = this;
            const url = `/api/groups/${this.group}/all.json`;
            axios.get(url).then(response => {
                _this.options = response.data.data;
                Event.$emit('GroupLoading', false);
            });
        },
        viewGroups() {
            const ids = this.$store.state.selectedIds.filter(n => n);
            this.$store.commit('setFilterType', "finder");
            this.$store.dispatch('loadFilteredGroups', { ids: ids, viewChange: true });
            Event.$emit('CloseGroupFinderModal');
        },
        optionLabel(option) {
            return parseInt(this.stepid) === 3 ? `Near ${option}` : option;
        },
        selectCategory(evt) {
            const input = evt.target;
            const selectedValue = input.value;

            if (selectedValue !== '#') {
                // If an options is selected endable NEXT button
                this.nextDisabled = false;
            }

            // Get ids and labels from store
            /* 
            let ids = this.$store.state.selectedIds;
             let labels = this.$store.state.selectedLabels;
             // Set selected label from select title
             this.selectedLabel = input.options[input.selectedIndex].title;
             labels[this.group] = input.options[input.selectedIndex].title;

             if (ids.includes(this.prevSelectedId)) {
                 const idx = ids.indexOf(this.prevSelectedId);
                 if (idx != -1) {
                     ids.splice(idx, 1);
                 }
             }

             if (selectedValue !== '#') {
                 // If an options is selected endable NEXT button
                 this.nextDisabled = false;
                 // Set previous selected id incase user returns to select to select another option.
                 this.prevSelectedId = selectedValue;
                 if (this.selectedId === selectedValue) {
                     if (!ids.includes(selectedValue)) {
                         ids.push(selectedValue);
                     }
                 } else {
                     if (!ids.includes(selectedValue)) {
                         if (ids.includes(this.selectedId)) {
                             const idx = ids.indexOf(this.selectedId);
                             if (idx != -1) {
                                 ids.splice(idx, 1);
                             }
                         }
                         ids.push(selectedValue);
                     }
                 }

                 this.$store.commit('setSelectedIds', ids);
                 this.$store.commit('setSelectedLabels', labels);

             } else {
                 if (ids.includes(this.prevSelectedId)) {
                     const idx = ids.indexOf(this.prevSelectedId);
                     if (idx != -1) {
                         ids.splice(idx, 1);
                     }
                 }
             }
             */


            Event.$emit('GroupLoading', true);
            // Prevents initial load of options Emitting UpdateGroupFilterOptions starting another AJAX request.
            if (this.initialLoad === 0) {
                Event.$emit('UpdateGroupFiltersOptions', this.stepid);
            } else {
                this.initialLoad = 0;
                Event.$emit('GroupLoading', false);
            }

            const selectedIDS = this.$store.state.selectedIds;
            this.$store.dispatch('loadFilteredGroups', { ids: selectedIDS, viewChange: false });

        },
        prev() {
            const prevStep = (this.currentStep - 1) !== 0 ? this.currentStep - 1 : 1;
            this.$store.commit('setStep', prevStep);
        },
        next() {
            const nextStep = (this.currentStep + 1) !== 4 ? this.currentStep + 1 : 3;
            this.$store.commit('setStep', nextStep);

            //const ids = this.$store.state.selectedIds
            //this.$store.dispatch('loadFilteredGroups', { ids: ids, viewChange: false });
        },
        searchGroups() {
            Event.$emit('CloseGroupFinderModal');
            Event.$emit('OpenGroupSearchModal');
        }
    },
    created() {
        const _this = this;
        _this.loadAllOptions();

        Event.$on('UpdateGroupFiltersOptions', (stid) => {
            if (_this.stepid !== 1 && _this.stepid !== stid) {
                let ids = _this.$store.state.selectedIds.filter(n => n);
                if (ids == 0) {
                    _this.loadAllOptions();
                } else {
                    _this.loadOptions();
                }
            }
        });

        // function setOption(selectElement, value) {
        //     return [...selectElement.options].some((option, index) => {
        //         if (option.value == value) {
        //             selectElement.selectedIndex = index;
        //             return true;
        //         }
        //     });
        // }

        Event.$on('SetGroupFiltersOption', (value) => {
            if (_this.stepid === 1) {
                _this.selectedId = value;
                _this.nextDisabled = false;
                _this.prevSelectedId = value;
            }
        });

        Event.$on('GroupPageLoading', (state) => {
            _this.groupsLoading = state;
        });
    }
}