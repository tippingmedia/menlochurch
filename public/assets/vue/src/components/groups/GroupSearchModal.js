export default {
    template: "#modal-group-search",
    delimiters: ['${', '}'],
    data() {
        return {
            isactive: false,
            query: ''
        }
    },
    computed: {
        classObject() {
            if (this.isactive) {
                return 'modal -active';
            }
            return 'modal';
        }
    },
    methods: {
        closeModal(event) {
            this.isactive = false;
        },
        closeModalFromBg(event) {
            const cl = event.target.classList;
            if (cl.contains('modal')) {
                this.isactive = false;
            }
        },
        groupFinder() {
            Event.$emit('OpenGroupFinderModal');
            this.isactive = false;
        },
        search(event) {
            //const query = event.target.querySelector('input').value;
            const query = this.query;
            this.$store.dispatch('searchFilteredGroups', query);
            this.$store.commit('setFilterType', "search");
            this.isactive = false;
        }
    },
    created() {
        const _this = this;
        Event.$on('OpenGroupSearchModal', () => {
            _this.isactive = true;
            _this.query = '';
        });
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                _this.isactive = false;
            }
        };
    }
}