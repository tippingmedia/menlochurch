import UpdateSelectsMixin from '../../mixins/SelectedIdsLabelsMixin';
export default {
    mixins: [UpdateSelectsMixin],
    template: "#groups-category-template",
    delimiters: ['${', '}'],
    props: ['category'],
    methods: {
        selectGroup(evt) {
            evt.preventDefault();
            // Current Locale corresponding Group Area
            const baseCampus = window.groupLocaleAreas[window.currentLocale];

            //this.$store.dispatch('loadFilteredGroups', { ids: [this.category.id, baseCampus.id], viewChange: true });
            //this.$store.commit('setFilterType', "category");
            //this.$store.commit('setSelectedGroupCategory', this.category.label);

            // setting selected Ids in store
            this.$store.commit('setSelectedIds', [this.category.id]);
            // Setting the groupCategory label in store
            this.$store.commit('setSelectedLabels', { groupCategory: this.category.label });
            // Updating Options of other selects
            Event.$emit('UpdateGroupFiltersOptions', 2);
            // Set the first step select to proper label
            Event.$emit('SetGroupFiltersOption', this.category.id);
            Event.$emit('GroupLoading', true);

            this.updateSelects();

            //const ids = this.$store.state.selectedIds
            //this.$store.dispatch('loadFilteredGroups', { ids: ids, viewChange: false });

            this.navigateToStep(1);
            window.scrollTo(0, 0);
        },
        navigateToStep(step) {
            Event.$emit('OpenGroupFinderModal');
            this.$store.commit('setStep', step);
        }
    }
}