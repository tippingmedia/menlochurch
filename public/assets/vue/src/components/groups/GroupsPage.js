import GroupCategory from "./GroupCategory";
import GroupFeatured from "./GroupFeatured";
import GroupsList from "./GroupsList";
import GroupSearchModal from "./GroupSearchModal";
import GroupFinderModal from "./GroupFinderModal";
import store from "../../store/groups";
import axios from "axios";
import scrollIntoView from "smoothscroll-polyfill";
import tabby from "tabby";

export default {
    template: "#groups-page-template",
    delimiters: ['${', '}'],
    store,
    data() {
        return {
            loading: true,
            featuredGroups: []
        }
    },
    computed: {
        viewlist() {
            return this.$store.state.view === "list" ? true : false;
        },
        viewfilter() {
            return this.$store.state.view === "filter" ? true : false;
        },
        viewdefault() {
            return this.$store.state.view === "default" ? true : false;
        },
        groupCategories() {
            return this.$store.state.groupCategories.sort((obj1, obj2) => { return obj1.label > obj2.label });
        },
        totalGroups() {
            return this.$store.state.filteredGroups.length;
        },
        selectedLabels() {
            return this.$store.state.selectedLabels;
        },
        filterType() {
            return this.$store.state.filterType;
        },
        searchQuery() {
            return this.$store.state.searchQuery;
        },
        selectedGroupCategory() {
            return this.$store.state.selectedGroupCategory;
        }
    },
    methods: {
        loadFeaturedGroups() {
            const _this = this;
            // Current Locale corresponding Group Area
            const baseCampus = window.groupLocaleAreas[window.currentLocale];
            const url = `/api/featuredgroups/${baseCampus.id}.json`;
            axios.get(url).then(response => {
                _this.featuredGroups = response.data.data;
            });
        },
        searchGroups() {
            Event.$emit('OpenGroupSearchModal');
        },
        navigateToStep(step) {
            Event.$emit('OpenGroupFinderModal');
            this.$store.commit('setStep', step);
        },
        setTabView() {
            this.$store.commit('setView', 'default');
        },
        scrollTo(evt) {
            let target = evt.target.href.split("#");
            document.querySelector("#" + target[1]).scrollIntoView({ behavior: 'smooth' });
        },
        scrollToCategories() {
            tabby.toggleTab('#tab-all', document.querySelector('.tabs_tab[data-tab][href*="#tab-all"]'));
            document.querySelector("#group-content").scrollIntoView({ behavior: 'smooth' });

        },
        scrollToFeatured() {
            tabby.toggleTab('#tab-featured', document.querySelector('.tabs_tab[data-tab][href*="#tab-featured"]'));
            document.querySelector("#group-content").scrollIntoView({ behavior: 'smooth' });

        }
    },
    components: {
        GroupCategory,
        GroupFeatured,
        GroupsList,
        GroupSearchModal,
        GroupFinderModal
    },
    created() {
        const _this = this;
        this.loadFeaturedGroups();
        this.$store.dispatch('loadGroupCategories');
        this.$store.dispatch('loadAllGroups');
        Event.$on('GroupPageLoading', (state) => {
            _this.loading = state;
        });
    }
}