import GroupSteps from './GroupSteps.vue';

export default {
    template: "#modal-group-finder",
    delimiters: ['${', '}'],
    data() {
        return {
            isactive: false
        }
    },
    computed: {
        classObject() {
            if (this.isactive) {
                return 'modal -active';
            }
            return 'modal';
        }
    },
    methods: {
        closeModal(event) {
            this.isactive = false;
        },
        closeModalFromBg(event) {
            const cl = event.target.classList;
            if (cl.contains('modal')) {
                this.isactive = false;
            }
        }
    },
    created() {
        const _this = this;
        Event.$on('CloseGroupFinderModal', () => {
            _this.isactive = false;
        });
        Event.$on('OpenGroupFinderModal', () => {
            _this.isactive = true;
        });
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                _this.isactive = false;
            }
        };
    },
    components: {
        GroupSteps
    }
}