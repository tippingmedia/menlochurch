export default {
    template: "#groups-featured-template",
    delimiters: ['${', '}'],
    props: ['group'],
    computed: {
        link() {
            return `#/modal/group/${this.group.id}/${this.group.locale}`;
        },
        bgStyleObject() {
            return {
                'background-image': `url(${this.group.imageUrl})`
            }
        },
        ga() {
            return `{"hitType":"event","eventCategory":"group","eventAction":"modal","eventLabel":"${this.group.title}"}`;
        }
    }
}