import GroupSet from './GroupSet';

export default {
    template: "#groups-list-template",
    delimiters: ['${', '}'],
    data() {
        return {
            daysOfWeek: [
                { day: 'Sunday', id: 0 },
                { day: 'Monday', id: 1 },
                { day: 'Tuesday', id: 2 },
                { day: 'Wednesday', id: 3 },
                { day: 'Thursday', id: 4 },
                { day: 'Friday', id: 5 },
                { day: 'Saturday', id: 6 }
            ]
        }
    },
    computed: {
        groupsByDay() {
            const _this = this;
            let groups = this.$store.state.filteredGroups;
            let groupSet = [];
            for (let dow of this.daysOfWeek) {
                let set = {};
                set.dayOfWeek = dow.day;
                set.groups = groups.filter(function(group) {
                    return parseInt(_this.$moment(group.groupBegins).format('d')) === dow.id;
                });
                groupSet.push(set);
            }
            return groupSet;
        }
    },
    components: {
        GroupSet
    }
}