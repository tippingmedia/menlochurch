import vue from 'vue';
import axios from 'axios';
import moment from 'moment';
import ListSection from './ListSection';

export default {
    template: "#filtered-section-template",
    delimiters: ['${', '}'],
    computed: {
        filterview() {
            if (this.$store.state.view === "filter") {
                return true;
            }
            return false;
        },
        filterId() {
            return this.$store.state.filterId;
        },
        filterLabel() {
            return this.$store.state.filterLabel;
        },
        filterType() {
            return this.$store.state.filterType;
        },
        filterTotal() {
            return this.$store.state.filterTotal;
        }
    },
    methods: {
        setInitalTotalPages() {
            const _this = this;
            return new Promise(function(resolve, reject) {
                axios.get(`${_this.api}${_this.filterId}.json`).then(response => {
                    _this.totalPages = response.data.meta.pagination.total_pages;
                    resolve(true);
                });
            });
        },
        backToList() {
            this.$store.commit("setView", "list");
            this.$store.commit("setFilterId", "null");
            this.$store.commit("setFilterLabel", "");
            this.$store.commit("setFilterType", "null");
        },
        latest(date) {
            const messageDate = moment(date);
            return moment().isBefore(messageDate.add(7, 'days'));
        },
        searchEntries(event) {
            event.preventDefault();
            Event.$emit('OpenFilterModal');
        }
    },
    components: {
        ListSection
    }
}