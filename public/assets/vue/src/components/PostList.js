import Vue from 'vue';
import axios from 'axios';
import moment from 'moment';

export default {
    template: "#list-template",
    delimiters: ['${', '}'],
    data: function() {
        return {
            nextPage: 0,
            showSpinner: 1,
            totalPages: 0,
            currentPage: 0,
            posts: [],
            apiurl: document.querySelector('#list-template').dataset.apiurl
        }
    },
    computed: {
        isactive() {
            if (this.$store.state.view === "list") {
                return true;
            }
            return false;
        }
    },
    methods: {
        loadMorePosts: function() {
            this.showSpinner = 1;
            if (this.nextPage <= this.totalPages) {
                axios.get(`${this.apiurl}?pg=${this.nextPage}`).then(response => {
                    this.posts = this.posts.concat(response.data.data);
                    this.currentPage = response.data.meta.pagination.current_page;
                    this.nextPage = this.nextPage + 1;

                    this.showSpinner = 0;
                });
            }
        },
        latest(date) {
            const messageDate = moment(date);
            return moment().isBefore(messageDate.add(7, 'days'));
        }
    },
    created: function() {
        if (this.apiurl !== "") {
            axios.get(`${this.apiurl}`).then(response => {
                this.totalPages = response.data.meta.pagination.total_pages;
                if (response.data.meta.pagination.total_pages > 1) {
                    this.nextPage = 2;
                }

                this.showSpinner = 0;
            });
        }
    }
}