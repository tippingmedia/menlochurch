import Vue from 'vue';
import moment from 'moment';
export default {
    template: '#list-card',
    delimiters: ['${', '}'],
    props: ['post', 'type'],
    computed: {
        hasImg() {
            let hasImg = false;
            if (this.type === 'person') {
                if (this.post.profileImageUrl !== "") {
                    hasImg = true;
                }
            }
            return hasImg;
        },

        staffCardClassObj() {
            let profileType = this.hasImg ? 'person-photo' : 'person-plain';
            return `person ratio ratio-2x3 block ${profileType}`;
        }
    },
    methods: {
        deadline(date) {
            const ddate = moment(date);
            const cdate = moment(date).subtract(7, 'days');
            return moment().isBetween(cdate, ddate);
        }
    },
    created() {
        this.$options.template = `#card-${this.type}`;
    }
}