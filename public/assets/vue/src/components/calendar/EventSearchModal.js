import CalendarCampus from './CalendarCampus.vue';
export default {
    template: '#search-modal-template',
    delimiters: ['${', '}'],
    data() {
        return {
            isactive: false,
            query: ''
        }
    },
    computed: {
        campuses() {
            return this.$store.state.campuses;
        },
        selectedCampus() {
            const _this = this;
            const _campus = this.$store.state.checkedCampuses;

            if ('null' === _campus) {
                return this.allCampuses.name;
            } else {
                //console.log(_campus);
                let campus = this.$store.state.campuses.filter(campus => {
                    //console.log(campus.slug);
                    if (campus.slug === _campus) {
                        return true;
                    }
                });
                if (campus.length > 0) {
                    return campus[0].name;
                } else {
                    return this.allCampuses.name;
                }
            }

        },
        currentCampus() {
            return this.$store.state.currentCampus;
        },
        allCampuses() {
            return {
                name: 'All Campuses',
                id: 'null',
                slug: 'null'
            };
        },
        classObject() {
            if (this.isactive) {
                return 'modal -active';
            }
            return 'modal';
        }
    },
    methods: {
        closeModal(event) {
            this.isactive = false;
        },
        closeModalFromBg(event) {
            const cl = event.target.classList;
            if (cl.contains('modal')) {
                this.isactive = false;
            }
        },
        search(event) {
            //const query = event.target.querySelector('input').value;
            const query = this.query;
            Event.$emit('SearchEvents', query);
            this.isactive = false;
        }
    },
    components: {
        CalendarCampus
    },
    created() {
        const _this = this;
        Event.$on('OpenEventSearchModal', () => {
            _this.isactive = true;
            _this.query = '';
        });
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                _this.isactive = false;
            }
        };
    }
}