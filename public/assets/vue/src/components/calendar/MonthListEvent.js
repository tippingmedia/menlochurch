export default {
    template: "#list-event-template",
    delimiters: ['${', '}'],
    props: ['event'],
    computed: {
        link() {
            return `#/modal/event/${this.event.eid}/${this.event.locale}`;
        },
        id() {
            if (this.$store.state.view === "search") {
                return `search-event-${this.event.eid}`;
            }
            return `list-event-${this.event.eid}`;
        },
        times() {
            if (this.event.allDay === 1) {
                return "All Day";
            }
            const startDate = this.$moment.tz(this.event.startDate, this.$moment.ISO_8601, "America/Los_Angeles");
            const endDate = this.$moment.tz(this.event.endDate, this.$moment.ISO_8601, "America/Los_Angeles");
            //console.log(startDate.format('h:mm a'));
            return `${startDate.format("h:mm a")} - ${endDate.format("h:mm a")}`;
        },
        day() {
            const startDate = this.$moment.tz(this.event.startDate, this.$moment.ISO_8601, "America/Los_Angeles");
            return startDate.format("D");
        },
        month() {
            const startDate = this.$moment.tz(this.event.startDate, this.$moment.ISO_8601, "America/Los_Angeles");
            return startDate.format("MMM");
        },
        year() {
            const startDate = this.$moment.tz(this.event.startDate, this.$moment.ISO_8601, "America/Los_Angeles");
            return startDate.format("YYYY");
        },
        campus() {
            const _this = this;
            let campus = this.$store.state.campuses.filter(campus => {
                if (campus.slug === _this.event.locale) {
                    return campus;
                }
            });
            return campus.name;
        },
        classLIObject() {
            let cobj = 'events_item block';
            const endDate = this.$moment.tz(this.event.endDate, this.$moment.ISO_8601, "America/Los_Angeles");
            if (this.$moment() > endDate) {
                cobj += ` -past`;
            }
            return cobj;
        },
        styleLIObject() {
            return `color:${this.event.color}`;
        },
        styleLabelObject() {
            return `background:${this.event.color}`;
        },
        bgStyleObject() {
            return {
                'background': `url(${this.event.eventImage})`
            }
        },
        issearchview() {
            if (this.$store.state.view === "search") {
                return true;
            }
            return false;
        },
    }
}