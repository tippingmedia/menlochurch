import vue from 'vue';
import axios from 'axios';
import EventSearchModal from './EventSearchModal';
import MonthListEvent from './MonthListEvent';
import Loader from './Loader.vue';
export default {
    template: "#event-search-template",
    delimiters: ['${', '}'],
    data() {
        return {
            filterId: '',
            filterLabel: '',
            searchQuery: '',
            events: []
        }
    },
    computed: {
        searchview() {
            if (this.$store.state.view === "search") {
                return true;
            }
            return false;
        },
        campuses() {
            const _this = this;
            const _campus = this.$store.state.checkedCampuses;
            let name = 'All Campuses';
            if (_campus !== 'null') {
                let campus = this.$store.state.campuses.filter(campus => {
                    if (campus.slug === _campus) {
                        return campus;
                    }
                });
                if (campus.length > 0) {
                    name = campus[0].name;
                }
            }

            return name;
        },
        listevents() {
            const _this = this;
            let events = this.events.sort(function(a, b) {
                return _this.$moment(a.startDate) - _this.$moment(b.startDate);
            })
            return events;
        }
    },
    methods: {
        backToList() {
            this.$store.commit("setView", "list");
            this.filterLabel = '';
            this.filterTotal = '';
            this.searchQuery = '';
        },
        searchEvents(event) {
            event.preventDefault();
            Event.$emit('OpenEventSearchModal');
        }
    },
    components: {
        EventSearchModal,
        MonthListEvent,
        Loader
    },
    created() {
        const _this = this;
        Event.$on('SearchEvents', (query) => {
            _this.events = [];
            _this.searchQuery = query;
            this.$store.commit('setView', 'search');
            Event.$emit('LoadStart', { id: 'search' });

            if (_this.$store.state.checkedCampuses !== 'null') {
                let url = `/api/searchEvents/${_this.$store.state.currentCampus}/${query}.json`;

                axios.get(url).then(response => {
                    _this.events = response.data.data;
                    Event.$emit('LoadStateDone', { id: 'search' });
                }).catch(response => {
                    console.log(response);

                });
            } else {
                for (let campus of _this.$store.state.campuses) {
                    // get all campuses instead of all campus dummy campus
                    if (campus.slug !== 'null') {
                        let url = `/api/searchEvents/${campus.slug}/${query}.json`;

                        axios.get(url).then(response => {
                            _this.events = _this.events.concat(response.data.data);
                            Event.$emit('LoadStateDone', { id: 'search' });
                        }).catch(response => {
                            console.log(response);

                        });
                    }
                }
            }
        });
    }
}