import CurrentMonth from './CurrentMonth.vue';
import FeaturedEvent from './FeaturedEvent.vue';
import MonthList from './MonthList';
import GroupsSelect from './GroupsSelect.vue';
import EventSearch from './EventSearch';
import Loader from './Loader.vue';
export default {
    template: "#calendar-app-template",
    delimiters: ['${', '}'],
    computed: {
        listview() {
            if (this.$store.state.view === "list") {
                return true;
            }
            return false;
        },
        month() {
            return this.$store.state.currentMonth;
        },
        year() {
            return this.$store.state.currentYear;
        },
        formatedDate() {
            return this.$moment(`${this.year}-${this.month}-1`, 'YYYY-M-D').format('MMM YYYY');
        },
        days() {
            // start of week
            const SUNDAY = 0;
            const MONDAY = 1;
            // end of week
            const SATURDAY = 6;

            // Generating all days in current month.
            let days = [];
            let currentDay = this.$moment(`${this.year}-${this.month}-1`, 'YYYY-M-D');
            do {
                days.push(currentDay);
                currentDay = this.$moment(currentDay).add(1, 'days');
                // zero based months jan = 0
            } while ((currentDay.month() + 1) === this.month);

            // Add previous days to start
            currentDay = this.$moment(days[0]);
            if (currentDay.day() !== SUNDAY) {
                do {
                    currentDay = this.$moment(currentDay).subtract(1, 'days');
                    days.unshift(currentDay);
                    // Sunday is == 0
                } while (currentDay.day() !== SUNDAY);
            }

            // Add following day to end.
            // Last day of the month
            currentDay = this.$moment(days[days.length - 1]);
            if (currentDay.day() !== SATURDAY) {
                do {
                    currentDay = this.$moment(currentDay).add(1, 'days');
                    days.push(currentDay);
                } while (currentDay.day() !== SATURDAY);
            }

            return days;
        },
        weeks() {
            let weeks = [];
            let week = [];

            for (let day of this.days) {
                week.push(day);
                if (week.length === 7) {
                    weeks.push(week);
                    week = [];
                }
            }
            return weeks;
        },
        featuredEvents() {
    
            const _uniq = function(prop) {
                if (prop)
                    return (ele, i, arr) => arr.map(ele => ele[prop]).indexOf(ele[prop]) === i
                else
                    return (ele, i, arr) => arr.indexOf(ele) === i
            }
            
            let events = this.$store.state.featuredEvents.filter(event => this.$moment(event.startDate).isAfter());
            
            return events.filter(_uniq('id'));
        },
        campuses() {
            const _this = this;
            const _campus = this.$store.state.checkedCampuses;
            let name = 'All Campuses';
            if (_campus !== 'null') {
                let campus = this.$store.state.campuses.filter(campus => {
                    if (campus.slug === _campus) {
                        return campus;
                    }
                });
                if (campus.length > 0) {
                    name = campus[0].name;
                }
            }

            return name;
        }
    },
    methods: {
        goToday(event) {
            event.preventDefault();
            this.$store.commit('setCurrentMonth', parseInt(this.$moment().format('M')));
            this.$store.commit('setCurrentYear', parseInt(this.$moment().format('YYYY')));
            this.$store.commit('setSelectedDay', this.$moment().format('YYYY-MM-DD'));
            // Losd featured and month events to lists based on checked groups
            this.$store.dispatch('loadMonthEvents', this.$store.state.checkedGroups);
            this.$store.dispatch('loadFeaturedEvents', this.$store.state.checkedGroups);
        },
        chooseCalendars(event) {
            event.preventDefault();
            Event.$emit('OpenCalendarGroupModal');
        },
        searchEvents(event) {
            event.preventDefault();
            Event.$emit('OpenEventSearchModal');
        }
    },
    components: {
        CurrentMonth,
        MonthList,
        FeaturedEvent,
        GroupsSelect,
        EventSearch,
        Loader
    },
    created() {
        if (this.$store.state.checkedCampuses !== null && this.$store.state.checkedCampuses !== window.currentLocale) {
            this.$store.commit('setCurrentCampus', window.currentLocale);
            this.$store.commit('setCheckedCampuses', window.currentLocale);
        }
        //  load current month & day
        this.$store.commit('setCurrentMonth', parseInt(this.$moment().format('M')));
        this.$store.commit('setCurrentYear', parseInt(this.$moment().format('YYYY')));
        this.$store.commit('setSelectedDay', this.$moment().format('YYYY-MM-DD'));

    },
    mounted: function() {
        this.$store.commit('setView', 'list');
        this.$store.dispatch('loadGroups');
        this.$store.dispatch('loadCampuses');

        if (this.$store.state.checkedGroups.length > 0 && this.$store.state.checkedCampuses.length > 0) {

            this.$store.dispatch('loadFeaturedEvents', { groups: this.$store.state.checkedGroups, campuses: this.$store.state.checkedCampuses });
            this.$store.dispatch('loadMonthEvents', { groups: this.$store.state.checkedGroups, campuses: this.$store.state.checkedCampuses });

        }
    }
}