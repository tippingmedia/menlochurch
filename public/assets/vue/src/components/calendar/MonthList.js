import MonthListEvent from './MonthListEvent';
export default {
    template: "#month-list-template",
    delimiters: ['${', '}'],
    props: ['month'],
    computed: {
        events() {
            const _this = this;
            // console.log(this.$store.state.currentMonth);
            // console.log(this.month);
            let currentSelectedMonth = this.$moment(`${this.$store.state.currentYear}-${this.$store.state.currentMonth}-01`);
            let events = this.$store.state.events;
            //.filter(event => this.$moment(event.startDate).isSame(currentSelectedMonth, 'month'));
            events = events.sort(function(a, b) {
                return _this.$moment(a.startDate) - _this.$moment(b.startDate);
            });
            
            return events;
        },
    },
    components: {
        MonthListEvent
    }
}