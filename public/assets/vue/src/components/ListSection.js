import Vue from 'vue';
import axios from 'axios';
import ListCard from './ListCard';

export default {
    template: "#list-section",
    delimiters: ['${', '}'],
    props: [
        'api',
        'tmp',
        'heading',
        'col',
        'type'
    ],
    data: function() {
        return {
            nextPage: 1,
            showSpinner: 1,
            totalPages: 1,
            currentPage: 1,
            posts: [],
        }
    },
    computed: {
        classObject() {
            let classes = "col col-12 col-sm-6";
            switch (parseInt(this.col)) {
                case 2:
                    classes = "col col-12 col-sm-6";
                    break;
                case 3:
                    classes = "col col-12 col-sm-6 col-md-4";
                    break;
                case 4:
                    classes = "col col-12 col-sm-6 col-md-3";
                    break;

            }
            return classes;
        },
        filterId() {
            return this.$store.state.filterId;
        },
        filterLabel() {
            return this.$store.state.filterLabel;
        },
        filterType() {
            return this.$store.state.filterType;
        },
        searchQuery() {
            return this.$store.state.searchQuery;
        }
    },
    methods: {
        loadMorePosts: function() {
            const _this = this;
            this.showSpinner = 1;
            if (this.nextPage <= this.totalPages) {
                //console.log(`${this.apiUrl()}?pg=${this.nextPage}`);
                axios.get(`${this.apiUrl()}?pg=${_this.nextPage}`).then(response => {
                    _this.posts = _this.posts.concat(response.data.data);
                    _this.totalPages = response.data.meta.pagination !== undefined ? response.data.meta.pagination.total_pages : null;
                    _this.$store.commit("setFilterTotal", _this.posts.length);
                    _this.currentPage = response.data.meta.pagination !== undefined ? response.data.meta.pagination.current_page : 1;
                    _this.nextPage = _this.currentPage + 1;


                    // console.log(_this.currentPage);
                    // console.log(response.data.meta.pagination.current_page);
                    // console.log(_this.totalPages);

                    _this.showSpinner = 0;
                });
            }
        },
        latest(date) {
            const messageDate = moment(date);
            return moment().isBefore(messageDate.add(7, 'days'));
        },
        apiUrl: function() {
            if (this.filterId !== 'null') {
                if (this.filterType == 'year') {
                    return `${this.api}year/${this.filterId}.json`;
                }
                return `${this.api}${this.filterId}.json`;
            }
            if (this.filterType == 'search') {
                return `${this.api}search/${this.searchQuery}.json`;
            }
            return this.api;
        },
        searchEntries(event) {
            event.preventDefault();
            Event.$emit('OpenFilterModal');
        }
    },
    created: function() {
        const _this = this;
        if (this.api !== "" && this.type == "list") {
            axios.get(`${this.apiUrl()}`).then(response => {
                _this.posts = this.posts.concat(response.data.data);
                _this.totalPages = parseInt(response.data.meta.pagination.total_pages);
                _this.$store.commit("setFilterTotal", _this.posts.length);
                _this.currentPage = 1;
                if (parseInt(response.data.meta.pagination.total_pages) > 1) {
                    _this.nextPage = 2;
                }
                // console.log(_this.totalPages);
                // console.log(_this.currentPage);
                // console.log(_this.nextPage);
                // console.log("--------------------");
                _this.showSpinner = 0;
            });
        }

        Event.$on('SearchPosts', query => {
            if (_this.type === 'search') {
                _this.nextPage = 1;
                _this.posts = [];
                _this.loadMorePosts();
            }
        });

        Event.$on('FilterPosts', query => {
            if (_this.type === 'filter') {
                _this.nextPage = 1;
                _this.posts = [];
                _this.loadMorePosts();
            }
        });
    },
    components: {
        ListCard
    }
}