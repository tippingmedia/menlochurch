import vue from 'vue';
import axios from 'axios';
import ListSection from './ListSection';

export default {
    template: "#search-section-template",
    delimiters: ['${', '}'],
    computed: {
        searchview() {
            if (this.$store.state.view === "search") {
                return true;
            }
            return false;
        },
        filterId() {
            return this.$store.state.filterId;
        },
        filterLabel() {
            return this.$store.state.filterLabel;
        },
        filterType() {
            return this.$store.state.filterType;
        },
        filterTotal() {
            return this.$store.state.filterTotal;
        },
        searchQuery() {
            return this.$store.state.searchQuery;
        }
    },
    methods: {
        backToList() {
            this.$store.commit("setView", "list");
            this.$store.commit("setFilterId", "null");
            this.$store.commit("setFilterLabel", "");
            this.$store.commit("setFilterType", "null");
            this.$store.commit("setSearchQuery", "null");
        },
        searchEntries(event) {
            event.preventDefault();
            Event.$emit('OpenFilterModal');
        }
    },
    components: {
        ListSection
    }
}