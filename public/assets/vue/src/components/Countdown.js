class Countdown {
    constructor(id, verbose = true, progress) {
        this.id = id;
        this.verbose = verbose;
        this.progress = progress;
        this.event = new CustomEvent("complete", {
            detail: {
                time: new Date(),
            },
            bubbles: true,
            cancelable: true
        });
    }

    getTimeRemaining(endtime) {
        var localTime = new Date(endtime); //this.convertUTCDateToLocalDate(new Date(endtime));
        var t = Date.parse(localTime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));

        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }



    to(endtime) {
        const _this = this;
        const clock = document.getElementById(this.id);
        const daysSpan = clock.querySelector('.days');
        const hoursSpan = clock.querySelector('.hours');
        const minutesSpan = clock.querySelector('.minutes');
        const secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            const t = _this.getTimeRemaining(endtime);


            if (_this.verbose && t.days !== -1 && t.hours !== -1 && t.minutes !== -1 && t.seconds !== -1) {

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
            }

            if (typeof _this.progress === "function") {
                _this.progress(t);
            }

            if (t.total <= 0) {
                clock.dispatchEvent(_this.event);
                clearInterval(timeinterval);
            }
        }

        updateClock();
        let timeinterval = setInterval(updateClock, 1000);
    }

    convertUTCDateToLocalDate(date) {
        //date.getTime();
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();

        newDate.setHours(hours - offset);

        return newDate;
    }
}

export { Countdown as default };