import vue from 'vue';
export default {
    template: "#filters-template",
    data() {
        return {
            isactive: false
        }
    },
    computed: {
        classObject() {
            if (this.isactive) {
                return 'modal -active';
            }
            return 'modal';
        }
    },
    methods: {
        filter(event) {
            const filterId = event.target.options[event.target.selectedIndex].value;
            const filterLabel = event.target.options[event.target.selectedIndex].text;
            const filterType = event.target.name;
            this.$store.commit("setFilterId", filterId);
            this.$store.commit("setFilterLabel", filterLabel);
            this.$store.commit("setFilterType", filterType);
            this.$store.commit('setView', "filter");

            Event.$emit('FilterPosts', { id: filterId, label: filterLabel, type: filterType });
            this.closeModal();
        },
        search(event) {
            const query = event.target.querySelector('input[name=q]').value;
            this.$store.commit("setFilterType", "search");
            this.$store.commit('setView', "search");
            this.$store.commit("setSearchQuery", query);

            Event.$emit('SearchPosts', query);
            this.closeModal();
        },
        closeModal(event) {
            this.isactive = false;
            this.$el.querySelector('input').value = "";
            for (let sel of this.$el.querySelectorAll('select')) {
                sel.selectedIndex = 0;
            }
        },
        closeModalFromBg(event) {
            const cl = event.target.classList;
            if (cl.contains('modal')) {
                this.isactive = false;
            }
        }
    },
    created() {
        const _this = this;
        Event.$on('OpenFilterModal', () => {
            _this.isactive = true;
        });
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                _this.isactive = false;
            }
        };
    }
}