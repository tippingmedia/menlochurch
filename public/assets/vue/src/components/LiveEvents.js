import Countdown from "./Countdown";
import moment from 'moment';
class LiveEvents {
    constructor(id) {
        const _this = this;
        this.times = window.LiveEvents.times;
        this.body = document.body;
        this.id = id;
        this.phase = 0; // 0 == tostart, 1 == toend(during event)
        this.pos = 0;
        this.startCountdown();
        const elm = document.getElementById(this.id);
        this.countdown = null;

        elm.addEventListener('complete', function() {
            //console.log("------------");
            //console.log("COMPLETE");
            _this.countdown = null;
            //window.document.removeEventListener('complete', elm);
            if (_this.phase !== 1) {
                _this.phase = 1;
                _this.body.dataset.live = true;
                _this.startCountdown();
            } else {
                // increase our position in times array.
                _this.phase = 0;
                _this.body.dataset.live = false;
                _this.pos = _this.pos + 1;
                _this.startCountdown();
            }

            // console.log("POS:" + _this.pos);
            // console.log("PHASE:" + _this.phase);
            // console.log("------------");
        }, false);
    }

    startCountdown() {
        if (this.times.length > 0) {
            let date = this.phase === 0 ? this.times[this.pos].start : this.times[this.pos].end;
            const nextEventElm = document.querySelector('.nextevent');
            if (nextEventElm !== null) {
                nextEventElm.innerText = moment(date).tz('America/New_York').format('ddd MMM D [@] hh:mm a');
            }
            this.countdown = new Countdown(this.id, true, (time) => {
                if ((parseInt(time.days) === 0 && parseInt(time.hours) === 0 && parseInt(time.minutes) <= 60) && document.body.dataset.live === "false") {
                    document.body.dataset.prelive = true;
                } else {
                    document.body.dataset.prelive = false;
                }
            }).to(date);

        }
    }
}

export { LiveEvents as default };