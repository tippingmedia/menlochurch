import Filters from './Filters';
import ListSection from './ListSection';
import FilteredSection from './FilteredSection';
import SearchSection from './SearchSection';
import store from '../store/listpage';

export default {
    template: '#list-template',
    store,
    computed: {
        listview() {
            if (this.$store.state.view === "list") {
                return true;
            }
            return false;
        }
    },
    methods: {
        searchEntries(event) {
            event.preventDefault();
            Event.$emit('OpenFilterModal');
        }
    },
    components: {
        ListSection,
        Filters,
        FilteredSection,
        SearchSection
    }
}