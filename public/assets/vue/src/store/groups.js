import Vue from 'vue';
import Vuex from 'vuex';
import _ from 'lodash';
Vue.use(Vuex);

import axios from 'axios';

export default new Vuex.Store({
    state: {
        step: 1,
        view: 'default',
        filterType: '',
        selectedIds: [],
        groups: [],
        filteredGroups: [],
        selectedLabels: {},
        groupCategories: [],
        searchQuery: '',
        selectedGroupCategory: ''
    },
    mutations: {
        setStep(state, payload) {
            state.step = payload;
        },
        setView(state, payload) {
            state.view = payload;
        },
        setSelectedIds(state, payload) {
            state.selectedIds = payload;
        },
        setGroups(state, payload) {
            state.groups = payload;
        },
        setFilteredGroups(state, payload) {
            state.filteredGroups = payload;
        },
        setSelectedLabels(state, payload) {
            state.selectedLabels = payload;
        },
        setGroupCategories(state, payload) {
            state.groupCategories = payload;
        },
        setFilterType(state, payload) {
            state.filterType = payload;
        },
        setSearchQuery(state, payload) {
            state.searchQuery = payload;
        },
        setSelectedGroupCategory(state, payload) {
            state.selectedGroupCategory = payload;
        }
    },
    actions: {
        loadFilteredGroups({ commit, state }, params = { ids: [], viewChange: true }) {
            Event.$emit('GroupPageLoading', true);

            let idChars = params.ids.filter(n => n);
            // make all ids INT
            let ids = idChars.map(function(x) {
                return parseInt(x, 10);
            });
            let filtered = state.groups.filter(group => {
                // make all ids INT
                let refs = group.refs.map(function(x) {
                    return parseInt(x, 10);
                });
                // if all ids are in refs return this group
                return _.difference(ids, refs).length === 0 ? true : false;
            });

            commit("setFilteredGroups", filtered);

            if (params.viewChange) {
                commit("setView", 'list');
            }

            Event.$emit('GroupPageLoading', false);

            // let currentCampus = window.campusBaseUrls[document.body.dataset.campus];
            // let baseUrl = document.body.dataset.campus !== "mlo" ? currentCampus : '';
            /** 
                let baseUrl = '';
                let ids = params.ids.filter(n => n);
                let url = ids.length > 0 ? `${baseUrl}/api/groups/${ids.toString()}.json` : `${baseUrl}/api/groups.json`;
                axios.get(url).then(response => {
                    commit("setFilteredGroups", response.data.data);
                    if (params.viewChange) {
                        commit("setView", 'list');
                    }
                    Event.$emit('GroupPageLoading', false);
                });
            **/
        },
        loadAllGroups({ commit, state }) {
            axios.get('/api/groups.json').then(response => {
                commit("setGroups", response.data.data);
            });
        },
        searchFilteredGroups({ commit, state }, query) {
            Event.$emit('GroupPageLoading', true);

            // let currentCampus = window.campusBaseUrls[document.body.dataset.campus];
            // let baseUrl = document.body.dataset.campus !== "mlo" ? currentCampus : '';
            let baseUrl = '';

            let url = `${baseUrl}/api/groups/search/${query}.json`;
            axios.get(url).then(response => {
                commit("setFilteredGroups", response.data.data);
                commit("setView", 'list');
                commit("setSearchQuery", query);
                Event.$emit('GroupPageLoading', false);
            });
        },
        loadGroupCategories({ commit, state }) {
            Event.$emit('GroupPageLoading', true);

            // let currentCampus = window.campusBaseUrls[document.body.dataset.campus];
            // let baseUrl = document.body.dataset.campus !== "mlo" ? currentCampus : '';
            let baseUrl = '';

            // const url = `${baseUrl}/api/groups/groupCategory/all.json`;
            const url = `${baseUrl}/api/groups/groupInterests/all.json`;
            axios.get(url).then(response => {
                commit('setGroupCategories', response.data.data);
                Event.$emit('GroupPageLoading', false);
            });
        }
    }
});