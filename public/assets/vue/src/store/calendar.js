import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import createPersistedState from 'vuex-persistedstate';
import moment from 'moment';
moment.tz.setDefault('UTC');

import axios from 'axios';

export default new Vuex.Store({

    state: {
        currentYear: moment().format('YYYY'),
        currentMonth: moment().format('M'),
        selectedDay: moment().format('YYYY-M-D'),
        events: [],
        featuredEvents: [],
        dayEvents: [],
        groups: [],
        campuses: [],
        checkedGroups: [1],
        currentCampus: window.currentLocale,
        checkedCampuses: window.currentLocale,
        view: 'list'
    },
    mutations: {
        setCurrentMonth(state, payload) {
            state.currentMonth = payload;
        },
        setCurrentYear(state, payload) {
            state.currentYear = payload;
        },
        setSelectedDay(state, payload) {
            state.selectedDay = payload;
        },
        setGroups(state, payload) {
            state.groups = payload;
        },
        setCheckedGroups(state, payload) {
            state.checkedGroups = payload;
        },
        setCampuses(state, payload) {
            state.campuses = payload;
        },
        setCheckedCampuses(state, payload) {
            state.checkedCampuses = payload;
        },
        setEvents(state, payload) {
            //state.events = payload;
            if (payload === null) {
                state.events = [];
            } else {
                state.events = state.events.concat(payload);
            }
        },
        setFeaturedEvents(state, payload) {
            if (payload === null) {
                state.featuredEvents = [];
            } else {
                state.featuredEvents = state.featuredEvents.concat(payload);
            }
        },

        setDayEvents(state, payload) {
            state.dayEvents = payload;
        },
        setCurrentCampus(state, payload) {
            state.currentCampus = payload;
        },
        setView(state, payload) {
            state.view = payload;
        }
    },
    actions: {
        loadMonthEvents({ commit, state }, data) {
            // Clear Events
            commit('setEvents', null);
            Event.$emit('LoadStart', { id: 'list' });

            let _monthStart = moment(`${state.currentYear}-${state.currentMonth}-1 00:00:00`, 'YYYY-M-D').format('YYYY-MM-DD');
            let _monthEnd = moment(`${state.currentYear}-${state.currentMonth}-1 23:59.59`, 'YYYY-M-D').endOf('month').format('YYYY-MM-DD');

            if (state.checkedCampuses !== 'null') {
                let url = `/api/monthEvents/${state.checkedGroups.join(',')}/${state.currentCampus}/${_monthStart}/${_monthEnd}.json`;

                axios.get(url).then(response => {
                    commit('setEvents', response.data.data);
                    // loading state
                    Event.$emit('LoadStateDone', { id: 'list' });

                }).catch(response => {
                    console.log(response);
                });

            } else {

                for (let campus of state.campuses) {
                    // get all campuses instead of all campus dummy campus
                    if (campus.slug !== 'null') {
                        let url = `/api/monthEvents/${state.checkedGroups.join(',')}/${campus.slug}/${_monthStart}/${_monthEnd}.json`;

                        axios.get(url).then(response => {
                            // set events
                            commit('setEvents', response.data.data);

                            Event.$emit('LoadStateDone', { id: 'list' });

                        }).catch(response => {
                            console.log(response);
                        });
                    }
                }
            }
        },

        loadFeaturedEvents({ commit, state }, data) {
            // Clear Featured Events
            commit('setFeaturedEvents', null);
            Event.$emit('LoadStart', { id: 'featured' });

            let _monthStart = moment(`${state.currentYear}-${state.currentMonth}-1 00:00:00`, 'YYYY-M-D').format('YYYY-MM-DD');
            let _monthEnd = moment(`${state.currentYear}-${state.currentMonth}-1 23:59.59`, 'YYYY-M-D').endOf('month').format('YYYY-MM-DD');

            if (state.checkedCampuses !== 'null') {
                let url = `/api/featuredEvents/${state.checkedGroups.join(',')}/${state.checkedCampuses}/${_monthStart}/${_monthEnd}.json`;

                axios.get(url).then(response => {
                    commit('setFeaturedEvents', response.data.data);

                    Event.$emit('LoadStateDone', { id: 'featured' });
                }).catch(response => {
                    console.log(response);
                });
            } else {

                for (let campus of state.campuses) {
                    // get all campuses instead of all campus dummy campus
                    if (campus.slug !== 'null') {

                        let url = `/api/featuredEvents/${state.checkedGroups.join(',')}/${campus.slug}/${_monthStart}/${_monthEnd}.json`;
                        axios.get(url).then(response => {
                            commit('setFeaturedEvents', response.data.data);
                            Event.$emit('LoadStateDone', { id: 'featured' });
                        }).catch(response => {
                            console.log(response);
                        });
                    }
                }
            }
        },

        loadGroups({ commit, state }) {
            let url = `/api/eventGroups`;

            axios.get(url).then(response => {
                commit('setGroups', response.data.groups);
            }).catch(response => {
                console.log(response);
            });
        },

        loadCampuses({ commit, state }) {
            let url = `/api/campuses.json`;
            let pageCampus = document.querySelector('body').dataset.campus;

            axios.get(url).then(response => {
                let data = response.data.data;
                commit('setCampuses', data);
            }).catch(response => {
                console.log(response);
            });
        }
    },

    plugins: [createPersistedState({ key: 'calendar' })]
});