import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        view: 'list',
        filterId: 'null',
        filterLabel: '',
        filterType: 'null',
        filterTotal: 0,
        searchQuery: 'null'
    },
    mutations: {
        setView(state, payload) {
            state.view = payload;
        },
        setFilterId(state, payload) {
            state.filterId = payload;
        },
        setFilterLabel(state, payload) {
            state.filterLabel = payload;
        },
        setFilterTotal(state, payload) {
            state.filterTotal = payload;
        },
        setFilterType(state, payload) {
            state.filterType = payload;
        },
        setSearchQuery(state, payload) {
            state.searchQuery = payload;
        }
    }
});