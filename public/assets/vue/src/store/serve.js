import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import axios from 'axios';

export default new Vuex.Store({
    state: {
        step: 1,
        view: 'list',
        selectedIds: [],
        filteredPosts: [],
        selectedLabels: {}
    },
    mutations: {
        setStep(state, payload) {
            state.step = payload;
        },
        setView(state, payload) {
            state.view = payload;
        },
        setSelectedIds(state, payload) {
            state.selectedIds = payload;
        },
        setFilteredPosts(state, payload) {
            state.filteredPosts = payload;
        },
        setSelectedLabels(state, payload) {
            state.selectedLabels = payload;
        },
    },
    actions: {
        loadFilteredPosts({ commit, state }, params = { ids: [], viewChange: true }) {
            var url = params.ids.length > 0 ? `api/serve/${params.ids}.json` : `api/serve.json`;
            axios.get(url).then(response => {
                commit("setFilteredPosts", response.data.data);
                if (params.viewChange) {
                    commit("setView", 'filter');
                }
            });
        },
        searchFilteredPosts({ commit, state }, query) {
            var url = `api/serve/search/${query}.json`;
            axios.get(url).then(response => {
                commit("setFilteredPosts", response.data.data);
                commit("setView", 'search');
            });
        },
    }
});