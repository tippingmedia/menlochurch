import Vue from 'vue';

import moment from 'moment-timezone';
moment.tz.setDefault('UTC');
Object.defineProperty(Vue.prototype, '$moment', {get() { return this.$root.moment; } });

import CalendarApp from './src/components/calendar/CalendarApp';
import store from './src/store/calendar';

import ListPage from './src/components/ListPage';
import truncate from 'truncate';

import GroupsPage from './src/components/groups/GroupsPage';


Vue.directive('truncate', {
    bind: function(el, binding, vnode) {
        var s = JSON.stringify
        el.innerHTML = truncate(s(binding.value), 80)
    }
});

const calendar = document.querySelector("#calendarApp");
if (calendar !== null) {
    window.Event = new Vue();
    new Vue({
        el: '#calendarApp',
        store,
        data: {
            moment
        },
        components: {
            CalendarApp
        }
    });
}

const listPage = document.querySelector('#listpage');
if (listPage !== null) {
    window.Event = new Vue();
    new Vue({
        el: '#listpage',
        delimiters: ['${', '}'],
        components: {
            ListPage
        }
    });
}

const groupsPage = document.querySelector('#groups-page');
if (groupsPage !== null) {
    window.Event = new Vue();
    new Vue({
        el: '#groups-page',
        delimiters: ['${', '}'],
        data: {
            moment
        },
        components: {
            GroupsPage
        }
    });
}