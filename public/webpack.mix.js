let mix = require('laravel-mix');
let SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

mix.js('./assets/js/menlo.js', './assets/dist/js/menlo.js');

if(!mix.inProduction()) {
    //console.log("FARRRRT");
    mix.webpackConfig({
        plugins: [
            new SVGSpritemapPlugin('assets/icons/24/*.svg', {
                output: {
                    filename : '../craft/templates/_partials/_icons.twig',
                    svgo : { removeTitle : true }
                },
                sprite: {
                    prefix : 'icon-24-',
                }
            })
        ] 
    });
}