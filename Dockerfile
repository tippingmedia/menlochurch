FROM alpine:edge

MAINTAINER Adam Randlett <adam@tippingmedia.com>

RUN apk --update --no-cache add \
	bash \
	nano \
	git \
	wget \
	unzip \
	openssh \
	php7 \
	php7-fpm \
	php7-ctype \
	php7-openssl\
	php7-zip \
	php7-pdo \
	php7-pdo_mysql \
	php7-pdo_pgsql \
	php7-mcrypt \
	php7-gd \
	php7-session \
	php7-openssl \
	php7-mbstring \
	php7-json \
	php7-curl \
	php7-dom \
	php7-exif \
	php7-posix \
	php7-opcache \
	php7-simplexml\
	php7-xml \
	php7-xmlreader \
    php7-xmlwriter \
	php7-intl \
	php7-fileinfo \
    php7-imagick

RUN mkdir /app
RUN echo 'root:docker' | chpasswd

COPY config/php.ini /etc/php7/conf.d/50-setting.ini

COPY config/php-fpm.conf /etc/php7/php-fpm.conf

WORKDIR /app

EXPOSE 9000

CMD ["php-fpm7", "-F"]